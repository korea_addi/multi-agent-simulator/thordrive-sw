from __future__ import annotations

import rospy  # type: ignore
from custom_msgs.msg import (  # type: ignore
    Localization,
    Nav,
    Reference,
    SemanticInfos,
)
from std_msgs.msg import String  # type: ignore


class Vehicle:
    _id: str
    _route_id: str | None
    _nav_fusion: Nav | None
    _pose_estimator: Localization | None
    _status: String | None
    _is_arrival: bool | None
    _semantic_infos: SemanticInfos | None

    def __init__(self, id: str):
        self._id = id
        self._nav_fusion = None
        self._pose_estimator = None
        self._semantic_infos = None
        self._route_id = None
        self._is_arrival = None
        self._status = None

        self._sub_nav_fusion = rospy.Subscriber(
            "/" + self._id + "/sensor/nav/nav_fusion",
            Nav,
            self._callback_nav_fusion,
            queue_size=1,
        )
        self._sub_pose_estimator = rospy.Subscriber(
            "/" + self._id + "/module/localization/pose_estimator",
            Localization,
            self._callback_pose_estimator,
            queue_size=1,
        )
        self._sub_semantic_info = rospy.Subscriber(
            "/" + self._id + "/module/map_reader/semantic_info",
            SemanticInfos,
            self._callback_semantic_info,
            queue_size=1,
        )
        self._sub_reference = rospy.Subscriber(
            "/" + self._id + "/module/globalpath_reader/reference",
            Reference,
            self._callback_reference,
            queue_size=1,
        )
        self._sub_status = rospy.Subscriber(
            "/" + self._id + "/module/commander/state/driving_mode",
            String,
            self._callback_status,
            queue_size=1,
        )

    @property
    def id(self) -> str:
        return self._id

    @property
    def route_id(self) -> str | None:
        return self._route_id

    @property
    def speed(self) -> float | None:
        return self._nav_fusion.speed if self._nav_fusion is not None else None

    @property
    def location(self) -> tuple[float, float] | None:
        return (
            (self._pose_estimator.x, self._pose_estimator.y)
            if self._pose_estimator is not None
            else None
        )

    @property
    def status(self) -> str | None:
        return self._status.data if self._status is not None else None

    def _callback_nav_fusion(self, msg: Nav):
        self._nav_fusion = msg

    def _callback_pose_estimator(self, msg: Localization):
        self._pose_estimator = msg

    def _callback_semantic_info(self, msg: SemanticInfos):
        self._semantic_infos = msg

    def _callback_reference(self, msg: Reference):
        self._route_id = msg.request_id
        self._is_arrival = msg.arrival_signal

    def _callback_status(self, msg: String):
        self._status = msg.data
