from __future__ import annotations

from typing import TYPE_CHECKING, Any

if TYPE_CHECKING:
    from thordrive_fms_v1.vehicle import Vehicle

import rospy  # type: ignore
from custom_msgs.msg import (  # type: ignore
    Goal,
)
from std_msgs.msg import Float32  # type: ignore


class VehiclePublisher:
    def __init__(self, vehicle: Vehicle):
        self._pub_route = rospy.Publisher(
            "/" + vehicle.id + "/module/globalpath_planner/goal",
            Goal,
            queue_size=1,
        )
        self._pub_speedlimit = rospy.Publisher(
            "/" + vehicle.id + "/module/new_velocity_planner/inputSpeed",
            Float32,
            queue_size=1,
        )

    def pub_route(self, route: Any):
        msg = Goal()
        msg.route = route
        self._pub_route.publish(msg)

    def pub_speedlimit(self, speed_limit: float):
        msg = Float32()
        msg.data = speed_limit
        self._pub_speedlimit.publish(msg)
