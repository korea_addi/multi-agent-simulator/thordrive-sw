from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from thordrive_fms_v1.publisher import VehiclePublisher
    from thordrive_fms_v1.vehicle import Vehicle


class FMS:
    _pub_hz: float
    _default_vehicle_ids: list[str]
    _default_route_ids: list[str]
    _input_speed: float

    _vehicles: list[Vehicle]
    _vehicle_publishers: list[VehiclePublisher]

    def __init__(self):
        self._pub_hz = 1.0
        self._default_vehicle_ids = ["thor01", "thor02", "thor03"]
        self._default_route_ids = ["1an", "2an", "3an"]
        self._input_speed = 30.0 / 3.6

        self._vehicles = []
        self._vehicle_publishers = []

    @property
    def pub_hz(self) -> float:
        return self._pub_hz

    @property
    def default_vehicle_ids(self) -> list[str]:
        return self._default_vehicle_ids

    def add_vehicle(self, vehicle: Vehicle, publisher: VehiclePublisher):
        self._vehicles.append(vehicle)
        self._vehicle_publishers.append(publisher)

    def pub_speedlimit(self, vehicle_publishers: list[VehiclePublisher]):
        for vehicle_publisher in vehicle_publishers:
            vehicle_publisher.pub_speedlimit(self._input_speed)

    def pub_route(
        self,
        vehicle_publishers: list[VehiclePublisher],
        route_ids: list[str],
    ):
        for vehicle_publisher, route_id in zip(vehicle_publishers, route_ids):
            vehicle_publisher.pub_route(route_id)

    def check_vehicle_route_should_updated(
        self,
    ) -> tuple[list[Vehicle], list[VehiclePublisher], list[str]]:
        vehicles_should_be_updated = []
        vehicle_publishers_should_be_updated = []
        route_ids_should_be_updated = []

        for vehicle, vehicle_publisher, route_id in zip(
            self._vehicles,
            self._vehicle_publishers,
            self._default_route_ids,
        ):
            if vehicle.route_id != route_id:
                vehicles_should_be_updated.append(vehicle)
                vehicle_publishers_should_be_updated.append(vehicle_publisher)
                route_ids_should_be_updated.append(route_id)

        return (
            vehicles_should_be_updated,
            vehicle_publishers_should_be_updated,
            route_ids_should_be_updated,
        )
