from __future__ import annotations

import rospy  # type: ignore

from thordrive_fms_v1.fms import FMS
from thordrive_fms_v1.publisher import VehiclePublisher
from thordrive_fms_v1.utils import visualize_vehicle_status
from thordrive_fms_v1.vehicle import Vehicle


def main():
    rospy.init_node("FMS", anonymous=True)
    fms = FMS()

    vehicle_ids = fms.default_vehicle_ids()

    for id in vehicle_ids:
        vehicle = Vehicle(id)
        vehicle_publisher = VehiclePublisher(vehicle)
        fms.add_vehicle(vehicle, vehicle_publisher)

    while not rospy.is_shutdown():
        (
            vehicles_should_be_updated,
            vehicle_publisher_should_be_updated,
            route_ids_should_be_updated,
        ) = fms.check_vehicle_route_should_updated()

        fms.pub_route(vehicle_publisher_should_be_updated, route_ids_should_be_updated)
        fms.pub_speedlimit(vehicle_publisher_should_be_updated)

        visualize_vehicle_status(fms._vehicles)

        rospy.sleep(fms.pub_hz)


main()
