from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from thordrive_fms_v1.vehicle import Vehicle


def visualize_vehicle_status(vehicles: list[Vehicle]):
    print("********** Autonomous Driving Cars **********")
    for vehicle in vehicles:
        print("Car ID    : ", vehicle.id)
        print("Route     : ", vehicle.route_id)
        print("Speed     : ", vehicle.speed)
        print("Location  : ", vehicle.location)
        print("Status    : ", vehicle.status)
        print("---------------------------------------------")
