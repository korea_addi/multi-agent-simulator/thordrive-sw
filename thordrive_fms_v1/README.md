# KETI-sim FMS

This repository provides a Fleet Management System (FMS) designed to oversee the movements of multiple vehicles effectively.


## 3rd Party Libraries

Dependencies for this project are managed using [Poetry](https://python-poetry.org), ensuring a streamlined and consistent development environment.


## Test Environment

The FMS has been tested and verified to work on the following platforms:

* Ubuntu 16.04
* Ubuntu 18.04
* Ubuntu 20.04


## Getting Started

**Clone the Repository**

To begin using the FMS, clone this repository and navigate to its directory:

``` bash
git clone https://gitlab.com/korea_addi/multi-agent-simulator/thordrive-fms.git
cd thordrive-fms
```

**Install Requirements**

Ensure Poetry is installed on your system. Then, navigate to the project directory and install the required dependencies:

```
cd thordrive_fms_v1
pip install poetry
poetry install
```


## Usage

**Running the Console**

To start the FMS console:

```
thordrive_fms_v1
```

## File Structure

```
.
├── README.md
├── pyproject.toml
└── thordrive_fms_v1
    ├── console.py
    ├── fms.py
    ├── publisher.py
    ├── utils.py
    └── vehicle.py
```
