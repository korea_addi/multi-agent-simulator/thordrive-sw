# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from custom_msgs/Goal.msg. Do not edit."""
import codecs
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import custom_msgs.msg
import geometry_msgs.msg
import std_msgs.msg

class Goal(genpy.Message):
  _md5sum = "5a6fb63e3863acbb9516baccb07cb33c"
  _type = "custom_msgs/Goal"
  _has_header = True  # flag to mark the presence of a Header object
  _full_text = """# daejin.hyeon@thordrive.ai (Daejin Hyeon)
# hvkim@thordrive.ai (Hanvit Kim)

Header header

#goal type
byte NODE_NAME=0
byte NODE_ID=1
byte UTM_POINT=2
byte LAT_LONG_POINT=3

string[] route 					# ['linkid1', 'linkid2', 'linkid3']

string request_id 				# request id
byte goal_type 					# one of goal types will be assgiend
string node_name 				# nominal station name 
string node_id 					# node id on semantic DB
geometry_msgs/Point utm_point 	# UTM coordination
custom_msgs/GeoPoint geopoint 	# lat long coordination
float64 destination_yaw 		# heading yaw

bool use_optional_destination 
byte goal_type2
string node_name2
string node_id2
geometry_msgs/Point utm_point2
custom_msgs/GeoPoint geopoint2
float64 destination_yaw2 		# heading yaw2

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
string frame_id

================================================================================
MSG: geometry_msgs/Point
# This contains the position of a point in free space
float64 x
float64 y
float64 z

================================================================================
MSG: custom_msgs/GeoPoint
# shhwang@thordrive.ai (Seunghyun Hwang)
float64 lat
float64 lon
"""
  # Pseudo-constants
  NODE_NAME = 0
  NODE_ID = 1
  UTM_POINT = 2
  LAT_LONG_POINT = 3

  __slots__ = ['header','route','request_id','goal_type','node_name','node_id','utm_point','geopoint','destination_yaw','use_optional_destination','goal_type2','node_name2','node_id2','utm_point2','geopoint2','destination_yaw2']
  _slot_types = ['std_msgs/Header','string[]','string','byte','string','string','geometry_msgs/Point','custom_msgs/GeoPoint','float64','bool','byte','string','string','geometry_msgs/Point','custom_msgs/GeoPoint','float64']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       header,route,request_id,goal_type,node_name,node_id,utm_point,geopoint,destination_yaw,use_optional_destination,goal_type2,node_name2,node_id2,utm_point2,geopoint2,destination_yaw2

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(Goal, self).__init__(*args, **kwds)
      # message fields cannot be None, assign default values for those that are
      if self.header is None:
        self.header = std_msgs.msg.Header()
      if self.route is None:
        self.route = []
      if self.request_id is None:
        self.request_id = ''
      if self.goal_type is None:
        self.goal_type = 0
      if self.node_name is None:
        self.node_name = ''
      if self.node_id is None:
        self.node_id = ''
      if self.utm_point is None:
        self.utm_point = geometry_msgs.msg.Point()
      if self.geopoint is None:
        self.geopoint = custom_msgs.msg.GeoPoint()
      if self.destination_yaw is None:
        self.destination_yaw = 0.
      if self.use_optional_destination is None:
        self.use_optional_destination = False
      if self.goal_type2 is None:
        self.goal_type2 = 0
      if self.node_name2 is None:
        self.node_name2 = ''
      if self.node_id2 is None:
        self.node_id2 = ''
      if self.utm_point2 is None:
        self.utm_point2 = geometry_msgs.msg.Point()
      if self.geopoint2 is None:
        self.geopoint2 = custom_msgs.msg.GeoPoint()
      if self.destination_yaw2 is None:
        self.destination_yaw2 = 0.
    else:
      self.header = std_msgs.msg.Header()
      self.route = []
      self.request_id = ''
      self.goal_type = 0
      self.node_name = ''
      self.node_id = ''
      self.utm_point = geometry_msgs.msg.Point()
      self.geopoint = custom_msgs.msg.GeoPoint()
      self.destination_yaw = 0.
      self.use_optional_destination = False
      self.goal_type2 = 0
      self.node_name2 = ''
      self.node_id2 = ''
      self.utm_point2 = geometry_msgs.msg.Point()
      self.geopoint2 = custom_msgs.msg.GeoPoint()
      self.destination_yaw2 = 0.

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_get_struct_3I().pack(_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs))
      _x = self.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      length = len(self.route)
      buff.write(_struct_I.pack(length))
      for val1 in self.route:
        length = len(val1)
        if python3 or type(val1) == unicode:
          val1 = val1.encode('utf-8')
          length = len(val1)
        buff.write(struct.Struct('<I%ss'%length).pack(length, val1))
      _x = self.request_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self.goal_type
      buff.write(_get_struct_b().pack(_x))
      _x = self.node_name
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self.node_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self
      buff.write(_get_struct_6dBb().pack(_x.utm_point.x, _x.utm_point.y, _x.utm_point.z, _x.geopoint.lat, _x.geopoint.lon, _x.destination_yaw, _x.use_optional_destination, _x.goal_type2))
      _x = self.node_name2
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self.node_id2
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self
      buff.write(_get_struct_6d().pack(_x.utm_point2.x, _x.utm_point2.y, _x.utm_point2.z, _x.geopoint2.lat, _x.geopoint2.lon, _x.destination_yaw2))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    if python3:
      codecs.lookup_error("rosmsg").msg_type = self._type
    try:
      if self.header is None:
        self.header = std_msgs.msg.Header()
      if self.utm_point is None:
        self.utm_point = geometry_msgs.msg.Point()
      if self.geopoint is None:
        self.geopoint = custom_msgs.msg.GeoPoint()
      if self.utm_point2 is None:
        self.utm_point2 = geometry_msgs.msg.Point()
      if self.geopoint2 is None:
        self.geopoint2 = custom_msgs.msg.GeoPoint()
      end = 0
      _x = self
      start = end
      end += 12
      (_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs,) = _get_struct_3I().unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.header.frame_id = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.header.frame_id = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.route = []
      for i in range(0, length):
        start = end
        end += 4
        (length,) = _struct_I.unpack(str[start:end])
        start = end
        end += length
        if python3:
          val1 = str[start:end].decode('utf-8', 'rosmsg')
        else:
          val1 = str[start:end]
        self.route.append(val1)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.request_id = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.request_id = str[start:end]
      start = end
      end += 1
      (self.goal_type,) = _get_struct_b().unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.node_name = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.node_name = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.node_id = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.node_id = str[start:end]
      _x = self
      start = end
      end += 50
      (_x.utm_point.x, _x.utm_point.y, _x.utm_point.z, _x.geopoint.lat, _x.geopoint.lon, _x.destination_yaw, _x.use_optional_destination, _x.goal_type2,) = _get_struct_6dBb().unpack(str[start:end])
      self.use_optional_destination = bool(self.use_optional_destination)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.node_name2 = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.node_name2 = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.node_id2 = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.node_id2 = str[start:end]
      _x = self
      start = end
      end += 48
      (_x.utm_point2.x, _x.utm_point2.y, _x.utm_point2.z, _x.geopoint2.lat, _x.geopoint2.lon, _x.destination_yaw2,) = _get_struct_6d().unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e)  # most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_get_struct_3I().pack(_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs))
      _x = self.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      length = len(self.route)
      buff.write(_struct_I.pack(length))
      for val1 in self.route:
        length = len(val1)
        if python3 or type(val1) == unicode:
          val1 = val1.encode('utf-8')
          length = len(val1)
        buff.write(struct.Struct('<I%ss'%length).pack(length, val1))
      _x = self.request_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self.goal_type
      buff.write(_get_struct_b().pack(_x))
      _x = self.node_name
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self.node_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self
      buff.write(_get_struct_6dBb().pack(_x.utm_point.x, _x.utm_point.y, _x.utm_point.z, _x.geopoint.lat, _x.geopoint.lon, _x.destination_yaw, _x.use_optional_destination, _x.goal_type2))
      _x = self.node_name2
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self.node_id2
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      buff.write(struct.Struct('<I%ss'%length).pack(length, _x))
      _x = self
      buff.write(_get_struct_6d().pack(_x.utm_point2.x, _x.utm_point2.y, _x.utm_point2.z, _x.geopoint2.lat, _x.geopoint2.lon, _x.destination_yaw2))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    if python3:
      codecs.lookup_error("rosmsg").msg_type = self._type
    try:
      if self.header is None:
        self.header = std_msgs.msg.Header()
      if self.utm_point is None:
        self.utm_point = geometry_msgs.msg.Point()
      if self.geopoint is None:
        self.geopoint = custom_msgs.msg.GeoPoint()
      if self.utm_point2 is None:
        self.utm_point2 = geometry_msgs.msg.Point()
      if self.geopoint2 is None:
        self.geopoint2 = custom_msgs.msg.GeoPoint()
      end = 0
      _x = self
      start = end
      end += 12
      (_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs,) = _get_struct_3I().unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.header.frame_id = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.header.frame_id = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      self.route = []
      for i in range(0, length):
        start = end
        end += 4
        (length,) = _struct_I.unpack(str[start:end])
        start = end
        end += length
        if python3:
          val1 = str[start:end].decode('utf-8', 'rosmsg')
        else:
          val1 = str[start:end]
        self.route.append(val1)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.request_id = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.request_id = str[start:end]
      start = end
      end += 1
      (self.goal_type,) = _get_struct_b().unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.node_name = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.node_name = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.node_id = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.node_id = str[start:end]
      _x = self
      start = end
      end += 50
      (_x.utm_point.x, _x.utm_point.y, _x.utm_point.z, _x.geopoint.lat, _x.geopoint.lon, _x.destination_yaw, _x.use_optional_destination, _x.goal_type2,) = _get_struct_6dBb().unpack(str[start:end])
      self.use_optional_destination = bool(self.use_optional_destination)
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.node_name2 = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.node_name2 = str[start:end]
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.node_id2 = str[start:end].decode('utf-8', 'rosmsg')
      else:
        self.node_id2 = str[start:end]
      _x = self
      start = end
      end += 48
      (_x.utm_point2.x, _x.utm_point2.y, _x.utm_point2.z, _x.geopoint2.lat, _x.geopoint2.lon, _x.destination_yaw2,) = _get_struct_6d().unpack(str[start:end])
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e)  # most likely buffer underfill

_struct_I = genpy.struct_I
def _get_struct_I():
    global _struct_I
    return _struct_I
_struct_3I = None
def _get_struct_3I():
    global _struct_3I
    if _struct_3I is None:
        _struct_3I = struct.Struct("<3I")
    return _struct_3I
_struct_6d = None
def _get_struct_6d():
    global _struct_6d
    if _struct_6d is None:
        _struct_6d = struct.Struct("<6d")
    return _struct_6d
_struct_6dBb = None
def _get_struct_6dBb():
    global _struct_6dBb
    if _struct_6dBb is None:
        _struct_6dBb = struct.Struct("<6dBb")
    return _struct_6dBb
_struct_b = None
def _get_struct_b():
    global _struct_b
    if _struct_b is None:
        _struct_b = struct.Struct("<b")
    return _struct_b
