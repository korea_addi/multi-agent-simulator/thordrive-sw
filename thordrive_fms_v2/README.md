# KETI-sim FMS

This repository provides a Fleet Management System (FMS) designed to efficiently oversee and manage the movements of multiple vehicles. It integrates seamlessly with the CARLA Simulator to simulate realistic vehicle behavior and supports various predefined scenarios, such as taxi and bus fleet operations.


## Features

- Fleet Management: Real-time management and coordination of multiple vehicles.
- CARLA Integration: Integration with the CARLA Simulator for realistic environment and vehicle interactions.
- Scenario Support: Predefined taxi and bus fleet scenarios for enhanced simulation and testing.


## 3rd Party Libraries

Dependencies are managed using [Poetry](https://python-poetry.org). All required libraries are listed in the `pyproject.toml` file.


## Test Environment

- Operating System: Ubuntu 20.04
- CARLA Version: 0.9.14
- Python Version: 3.8 or higher


## Getting Started

**Clone the Repository**

To start using the FMS, clone the repository and navigate to the project directory:

``` bash
git clone https://gitlab.com/korea_addi/multi-agent-simulator/thordrive-fms.git
cd thordrive-fms
```

**Install Requirements**

Ensure that Poetry is installed on your system. Then, navigate to the project folder and install the required dependencies:

```
cd thordrive_fms_v2
pip install poetry
poetry install
```


## Usage

### Run the CARLA Simulator

Start the CARLA Simulator using Docker. Ensure Docker is installed and GPU support is configured:

```
docker run --privileged --gpus all --net=host -e DISPLAY=$DISPLAY carlasim/carla:0.9.14 /bin/bash ./CarlaUE4.sh
```

### Run the FMS Server

Start the Fleet Management System (FMS) server:

```
thordrive_fms_v2_server
```


### Scenarios 

#### Taxi Scenario

**1. Start the Taxi Module:**

```
thordrive_fms_v2_taxi
```

**2. Start the Pedestrian Module:**

```
thordrive_fms_v2_pedestrian
```


#### Bus Scenario

**1. Start the Bus Module:**

```
thordrive_fms_v2_bus
```


## Notes

- Ensure that Docker is properly installed and GPU support is enabled for running the CARLA Simulator.
- For more detailed dependency specifications, refer to the `pyproject.toml` file.
- Adjust Docker configurations as needed to match your environment.
