from __future__ import annotations

import carla  # type: ignore
import rospy  # type: ignore

from thordrive_fms_v2.thor_carla_ros.api import CarlaAPI
from thordrive_fms_v2.thor_carla_ros.fsm.action.vehicle import (
    generate_taxi_return_actions,
)
from thordrive_fms_v2.thor_carla_ros.render import PyGameRender
from thordrive_fms_v2.thor_carla_ros.utils import generate_random_name
from thordrive_fms_v2.thor_carla_ros.vehicle import ThorTaxi


def main():
    name = generate_random_name()
    location = carla.Location(30, 7, 2)
    return_node_id = "1189012300"

    CarlaAPI.connect(ip="localhost", port=2000)
    spawn_tf = CarlaAPI.get_vehicle_spawn_point(location)

    thor_client = ThorTaxi(name, spawn_tf, hz=10)
    renderer = PyGameRender(thor_client._carla_actor, name)

    thor_client.initialize(topic_remap=True)

    actions = generate_taxi_return_actions(return_node_id)
    thor_client.set_actions(actions)

    while not rospy.is_shutdown():
        renderer.render()
        thor_client.process()


if __name__ == "__main__":
    main()
