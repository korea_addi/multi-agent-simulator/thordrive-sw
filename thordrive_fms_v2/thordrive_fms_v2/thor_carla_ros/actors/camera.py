from __future__ import annotations

import weakref

import carla  # type: ignore

from thordrive_fms_v2.thor_carla_ros.api import CarlaAPI


class Camera:
    _sensor: carla.Actor
    _image: carla.Image

    def __init__(self, actor: carla.Actor, width=640, height=480):
        extent = actor.bounding_box.extent
        location = carla.Location(x=extent.x - 3.0, z=extent.z * 2 + 3.0)
        rotation = carla.Rotation(pitch=-25.0)
        transform = carla.Transform(location, rotation)

        bp = CarlaAPI.find_blueprint("sensor.camera.rgb")
        bp.set_attribute("image_size_x", f"{width}")
        bp.set_attribute("image_size_y", f"{height}")

        self._sensor = CarlaAPI.spawn_actor(transform, bp, attach_to=actor)
        self._image = None

        weak_self = weakref.ref(self)
        self._sensor.listen(lambda data: Camera.callback(weak_self, data))

    def get_image(self) -> carla.Image | None:
        return self._image

    def __del__(self):
        if self._sensor.is_listening:
            self._sensor.stop()
        if self._sensor is not None:
            self._sensor.destroy()

    @staticmethod
    def callback(weak_self, image: carla.Image):
        self = weak_self()
        image.convert(carla.ColorConverter.Raw)
        self._image = image
