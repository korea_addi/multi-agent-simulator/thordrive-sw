from __future__ import annotations

import carla  # type: ignore

from thordrive_fms_v2.thor_carla_ros.actors.actor import CarlaActor


class CarlaVehicle(CarlaActor):
    def __init__(
        self,
        spawn_bp: carla.ActorBlueprint,
        spawn_tf: carla.Transform,
    ):
        super().__init__(spawn_bp, spawn_tf)
        self._reset_control()

    def apply_control(self, throttle: float, steer: float, brake: float):
        control = carla.VehicleControl(
            throttle=throttle,
            steer=steer,
            brake=brake,
            manual_gear_shift=False,
        )
        self._actor.apply_control(control)

    def open_door_front_right(self):
        self._actor.open_door(carla.VehicleDoor.FR)

    def open_door_back_right(self):
        self._actor.open_door(carla.VehicleDoor.RR)

    def close_door_front_right(self):
        self._actor.close_door(carla.VehicleDoor.FR)

    def close_door_back_right(self):
        self._actor.close_door(carla.VehicleDoor.RR)

    def _reset_control(self):
        control = carla.VehicleControl(
            throttle=0.0,
            steer=0.0,
            brake=0.0,
            manual_gear_shift=True,
            gear=1,
        )
        self._actor.apply_control(control)
