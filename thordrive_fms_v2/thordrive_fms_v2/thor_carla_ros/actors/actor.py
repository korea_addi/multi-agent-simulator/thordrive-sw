from __future__ import annotations

import carla  # type: ignore

from thordrive_fms_v2.thor_carla_ros.actors.camera import Camera
from thordrive_fms_v2.thor_carla_ros.api import CarlaAPI


class CarlaActor:
    _actor: carla.Actor
    _camera: Camera

    def __init__(self, spawn_bp: carla.ActorBlueprint, spawn_tf: carla.Transform):
        self._actor = self._try_spawn(spawn_bp, spawn_tf)
        self._camera = Camera(self._actor)

    @property
    def actor_id(self) -> int:
        return self._actor.id

    @property
    def location(self) -> tuple[float, float, float]:
        loc = self._actor.get_location()
        return loc.x, loc.y, loc.z

    @property
    def image(self) -> carla.Image | None:
        return self._camera.get_image()

    @property
    def other_actors(self) -> list[carla.Actor]:
        all_actors = CarlaAPI.world.get_actors()
        return [actor for actor in all_actors if actor.id != self._actor.id]

    def _try_spawn(self, spawn_bp: carla.ActorBlueprint, spawn_tf: carla.Transform):
        actor = CarlaAPI.try_spawn_actor(transform=spawn_tf, bp=spawn_bp)
        if actor is not None:
            return actor
        raise Exception("Failed to spawn actor.")

    def __del__(self):
        if self._actor.is_alive:
            self._actor.destroy()
