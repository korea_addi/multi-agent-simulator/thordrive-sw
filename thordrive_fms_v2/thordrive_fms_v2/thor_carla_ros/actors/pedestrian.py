from __future__ import annotations

import secrets

import carla  # type: ignore

from thordrive_fms_v2.thor_carla_ros.actors.actor import CarlaActor
from thordrive_fms_v2.thor_carla_ros.api import CarlaAPI


class CarlaPedestrian(CarlaActor):
    def __init__(self, spawn_tf: carla.Transform):
        spawn_bps = CarlaAPI.blueprint_library.filter("pedestrian")
        spawn_bp = secrets.choice(spawn_bps)
        super().__init__(spawn_bp, spawn_tf)

    def hide(self):
        self._actor.hide_pose()

    def show(self, x: float, y: float, z: float):
        tf = carla.Transform(carla.Location(x, y, z))
        self._actor.set_transform(tf)
        self._actor.show_pose()
