from __future__ import annotations

import carla  # type: ignore
import numpy as np
from scipy.spatial.transform import Rotation as R


def get_location_from_matrix(matrix: np.ndarray) -> carla.Location:
    return carla.Location(matrix[0, -1], matrix[1, -1], matrix[2, -1])


def get_rotation_from_matrix(matrix: np.ndarray) -> carla.Rotation:
    rotation = R.from_matrix(matrix[:3, :3])
    roll, pitch, yaw = rotation.as_euler("xyz", degrees=True)
    return carla.Rotation(pitch, yaw, roll)


def get_list_matrix(matrix):
    return [
        matrix[0][0],
        matrix[0][1],
        matrix[0][2],
        matrix[1][0],
        matrix[1][1],
        matrix[1][2],
        matrix[2][0],
        matrix[2][1],
        matrix[2][2],
        matrix[0][3],
        matrix[1][3],
        matrix[2][3],
    ]


def get_transform_from_matrix(matrix: np.ndarray) -> carla.Transform:
    return carla.Transform(
        get_location_from_matrix(matrix),
        get_rotation_from_matrix(matrix),
    )


def get_quaternion_from_rotation(rotation: carla.Rotation) -> np.ndarray:
    rotation = R.from_euler(
        seq="xyz",
        angles=[rotation.roll, rotation.pitch, rotation.yaw],
        degrees=True,
    )
    return rotation.as_quat()


def get_relative_transform(
    base: carla.Transform,
    target: carla.Transform,
) -> carla.Transform:
    inverse_matrix = base.transform.get_inverse_matrix()
    target_matrix = target.transform.get_matrix()
    relative_matrix = np.dot(inverse_matrix, target_matrix)
    return get_transform_from_matrix(relative_matrix)
