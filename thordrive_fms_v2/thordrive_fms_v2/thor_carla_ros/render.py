from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from thordrive_fms_v2.thor_carla_ros.actors.actor import CarlaActor


import numpy as np
import pygame


class PyGameRender:
    _display: pygame.Surface
    _carla_actor: CarlaActor

    def __init__(self, carla_actor: CarlaActor, name: str, width=640, height=480):
        pygame.init()
        pygame.display.set_caption(name)

        self._display = pygame.display.set_mode(
            (width, height), pygame.HWSURFACE | pygame.DOUBLEBUF
        )
        self._carla_actor = carla_actor

    def render(self):
        if self._carla_actor.image is None:
            return

        image = self._carla_actor.image
        array = np.frombuffer(image.raw_data, dtype=np.dtype("uint8"))
        array = np.reshape(array, (image.height, image.width, 4))
        array = array[:, :, :3]
        array = array[:, :, ::-1]

        surface = pygame.surfarray.make_surface(array.swapaxes(0, 1))

        if surface is not None:
            self._display.blit(surface, (0, 0))
            pygame.display.flip()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                raise KeyboardInterrupt

    def __del__(self):
        pygame.quit()
