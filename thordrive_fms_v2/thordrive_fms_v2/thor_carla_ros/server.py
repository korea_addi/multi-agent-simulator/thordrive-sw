from __future__ import annotations

import secrets

import carla  # type: ignore
import rospy  # type: ignore

from thordrive_fms_v2.thor_carla_ros.api import CarlaAPI
from thordrive_fms_v2.thor_carla_ros.fsm.action.vehicle import generate_taxi_actions
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.bus import (
    download_registered_bus_names,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.master import (
    allocate_pedestrian_to_taxi,
    download_taxi_waiting_pedestrian_names_from_server,
    select_idle_taxi_name_from_server,
    unregister_master,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.pedestrian import (
    download_pedestrian_profile_from_server,
    download_registered_pedestrian_ids,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.taxi import (
    download_registered_taxi_names,
)


class CarlaServer:
    def __init__(
        self,
        map_name: str = "Town03",
        ip: str = "localhost",
        port: int = 2000,
    ):
        CarlaAPI.connect(ip, port, map_name)
        CarlaAPI.set_sync(0.032)

    def create_environment(self):
        try_spawn_count = 10
        spawn_points = CarlaAPI.get_vehicle_spawn_points()
        for _ in range(try_spawn_count):
            tf = secrets.choice(spawn_points)
            actor = CarlaAPI.try_spawn_actor(tf, bp_wildcard_pattern="vehicle")
            if actor:
                actor.set_autopilot()

    def set_environment(self):
        world = CarlaAPI.world

        to_remove = []
        # to_remove.extend(world.get_environment_objects(carla.CityObjectLabel.Buildings))
        to_remove.extend(world.get_environment_objects(carla.CityObjectLabel.Fences))
        to_remove.extend(world.get_environment_objects(carla.CityObjectLabel.Other))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Pedestrians))
        to_remove.extend(world.get_environment_objects(carla.CityObjectLabel.Poles))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.RoadLines))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Roads))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Sidewalks))
        to_remove.extend(
            world.get_environment_objects(carla.CityObjectLabel.TrafficSigns)
        )
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Vegetation))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Car))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Bus))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Truck))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Motorcycle))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Bicycle))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Rider))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Train))
        # to_remove.extend(world.get_environment_objects(carla.CityObjectLabel.Walls))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Sky))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Ground))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Bridge))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.RailTrack))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.GuardRail))
        to_remove.extend(
            world.get_environment_objects(carla.CityObjectLabel.TrafficLight)
        )
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Static))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Dynamic))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Water))
        # objects.extend(world.get_environment_objects(carla.CityObjectLabel.Terrain))

        object_ids = {obj.id for obj in to_remove}
        world.enable_environment_objects(object_ids, False)

        special_objects = [
            object
            for object in CarlaAPI.world.get_environment_objects(
                carla.CityObjectLabel.Buildings
            )
            if -70 < object.transform.location.x < 0
            and 0 < object.transform.location.y < 30
        ]
        special_objects_ids = {obj.id for obj in special_objects}
        world.enable_environment_objects(special_objects_ids, False)

    def tick(self):
        CarlaAPI.tick()


class ROSServer(CarlaServer):
    _taxi_names: set[str]
    _bus_names: set[str]
    _passenger_ids: set[int]

    taxi_return_node_id: str

    def __init__(
        self,
        name: str,
        hz: int,
        map_name: str = "Town03",
        ip: str = "localhost",
        port: int = 2000,
    ):
        super().__init__(map_name, ip, port)

        rospy.init_node(name)
        self._rate = rospy.Rate(hz)

        unregister_master()

        self._taxi_names = download_registered_taxi_names()
        self._bus_names = download_registered_bus_names()
        self._passenger_ids = download_registered_pedestrian_ids()

    def process(self):
        self._check_new_bus()
        self._check_new_taxi()
        self._check_new_pedestrian()
        self._check_destroyed_bus()
        self._check_destroyed_taxi()
        self._check_destroyed_pedestrian()

        self._process_taxi_scenario()

    def sleep(self):
        self._rate.sleep()

    def set_taxi_return_node_id(self, node_id: str):
        self.taxi_return_node_id = node_id

    def _process_taxi_scenario(self):
        waiting_pedestrian_names = download_taxi_waiting_pedestrian_names_from_server()
        waiting_pedestrian_profiles = [
            download_pedestrian_profile_from_server(name)
            for name in waiting_pedestrian_names
        ]
        filtered_pedestrian_profiles = [
            profile for profile in waiting_pedestrian_profiles if profile is not None
        ]

        if not filtered_pedestrian_profiles:
            return

        idle_taxi_name = select_idle_taxi_name_from_server()

        if not idle_taxi_name:
            return

        selected_pedestrian_profile = filtered_pedestrian_profiles[0]

        actions = generate_taxi_actions(
            selected_pedestrian_profile.start_node_id,
            selected_pedestrian_profile.goal_node_id,
            self.taxi_return_node_id,
        )
        allocate_pedestrian_to_taxi(
            selected_pedestrian_profile.name, idle_taxi_name, actions
        )

    # Bus 등록
    def _check_new_bus(self):
        names = download_registered_bus_names()
        names = names - self._bus_names

        for name in names:
            self._bus_names.add(name)
            rospy.loginfo("Registering bus %s", name)

    # Bus 제거
    def _check_destroyed_bus(self):
        names = download_registered_bus_names()
        destroy_names = self._bus_names - names

        for name in destroy_names:
            self._bus_names.remove(name)
            rospy.loginfo("Unregistering bus %s", name)

    # Taxi 등록
    def _check_new_taxi(self):
        names = download_registered_taxi_names()
        names = names - self._taxi_names

        for name in names:
            self._taxi_names.add(name)
            rospy.loginfo("Registering taxi %s", name)

    # Taxi 제거
    def _check_destroyed_taxi(self):
        names = download_registered_taxi_names()
        destoryed_names = self._taxi_names - names

        for name in destoryed_names:
            self._taxi_names.remove(name)
            rospy.loginfo("Unregistering taxi %s", name)

    # Passenger 등록
    def _check_new_pedestrian(self):
        new_pedestrian_ids = download_registered_pedestrian_ids()
        new_pedestrian_ids = new_pedestrian_ids - self._passenger_ids

        for id in new_pedestrian_ids:
            self._passenger_ids.add(id)
            rospy.loginfo("Registering passenger %s", id)

    # Passenger 제거
    def _check_destroyed_pedestrian(self):
        pedestrian_ids = download_registered_pedestrian_ids()
        destoryed_pedestrian_ids = self._passenger_ids - pedestrian_ids

        for id in destoryed_pedestrian_ids:
            self._passenger_ids.remove(id)
            rospy.loginfo("Unregistering passenger %s", id)

    def __del__(self):
        unregister_master()
