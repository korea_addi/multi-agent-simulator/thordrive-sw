from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from thordrive_fms_v2.thor_carla_ros.pedestrian import ThorPedestrian

import rospy  # type: ignore

from thordrive_fms_v2.thor_carla_ros.fsm.profile.pedestrian import (
    deserialize,
    serialize,
)


def register_pedestrian(pedestrian: ThorPedestrian):
    new_name = pedestrian.name
    rospy.set_param(f"carla_master/pedestrians/{new_name}", dict())


def unregister_pedestrian(pedestrian: ThorPedestrian):
    name = pedestrian.name
    if rospy.has_param(f"carla_master/pedestrians/{name}"):
        rospy.delete_param(f"carla_master/pedestrians/{name}")


def download_registered_pedestrian_ids() -> set[int]:
    if rospy.has_param("carla_master/pedestrians"):
        return set(rospy.get_param("carla_master/pedestrians").keys())
    return set()


def upload_pedestrian_profile_to_server(pedestrian: ThorPedestrian):
    name = pedestrian.name
    profile = pedestrian.get_profile()
    rospy.set_param(f"carla_master/pedestrians/{name}/profile", serialize(profile))


def download_pedestrian_profile_from_server(name: str):
    if rospy.has_param(f"carla_master/pedestrians/{name}/profile"):
        raw_data = rospy.get_param(f"carla_master/pedestrians/{name}/profile")
        return deserialize(raw_data)
    return None


def is_pedestrian_onboard(pedestrian_name: str) -> bool:
    if rospy.has_param(f"carla_master/pedestrians/{pedestrian_name}/onboard"):
        return rospy.get_param(f"carla_master/pedestrians/{pedestrian_name}/onboard")
    return False


def set_pedestrian_onboard_to_server(pedestrian: ThorPedestrian):
    name = pedestrian.name
    rospy.set_param(f"carla_master/pedestrians/{name}/onboard", True)


def set_pedestrian_offboard_to_server(pedestrian: ThorPedestrian):
    name = pedestrian.name
    rospy.set_param(f"carla_master/pedestrians/{name}/onboard", False)
