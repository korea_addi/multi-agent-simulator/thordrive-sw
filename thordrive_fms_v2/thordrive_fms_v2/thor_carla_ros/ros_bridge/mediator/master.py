from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from thordrive_fms_v2.thor_carla_ros.fsm.action.vehicle import (
        Action as VehicleAction,
    )

import rospy  # type: ignore

from thordrive_fms_v2.thor_carla_ros.fsm.state.vehicle import State as VehicleState
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.pedestrian import (
    download_registered_pedestrian_ids,
)


def unregister_master():
    if rospy.has_param("carla_master"):
        rospy.delete_param("carla_master")


def allocate_pedestrian_to_taxi(
    pedestrian_name: str, taxi_name: str, actions: list[VehicleAction]
):
    from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.taxi import (
        upload_taxi_actions,
    )

    if not rospy.has_param(f"carla_master/services/taxi/{taxi_name}/pedestrian"):
        allocate_pedestrian_name_to_taxi(pedestrian_name, taxi_name)
        upload_taxi_actions(taxi_name, actions)


def allocate_pedestrian_name_to_taxi(pedestrian_name: str, taxi_name: str):
    rospy.set_param(
        f"carla_master/services/taxi/{taxi_name}/pedestrian", pedestrian_name
    )


def download_assigned_taxi_name_on_pedestrian_name_from_server(pedestrian_name: str):
    if rospy.has_param("carla_master/services/taxi"):
        taxis = rospy.get_param("carla_master/services/taxi")
        for taxi_name, taxi_data in taxis.items():
            if "pedestrian" in taxi_data and taxi_data["pedestrian"] == pedestrian_name:
                return taxi_name
    return None


def download_assigned_pedestrian_name_on_taxi_from_server(taxi_name: str):
    if rospy.has_param(f"carla_master/services/taxi/{taxi_name}/pedestrian"):
        return rospy.get_param(f"carla_master/services/taxi/{taxi_name}/pedestrian")
    return None


def download_taxi_waiting_pedestrian_names_from_server():
    ids = download_registered_pedestrian_ids()
    assigned_ids = download_assigned_pedestrian_name_from_server()
    return ids - assigned_ids


def download_assigned_pedestrian_name_from_server():
    if rospy.has_param("carla_master/services/taxi"):
        taxis = rospy.get_param("carla_master/services/taxi")
        return set(
            [
                taxi_data["pedestrian"]
                for taxi_data in taxis.values()
                if "pedestrian" in taxi_data
            ]
        )
    return set()


def deallocate_pedestrian_from_taxi(taxi_name: str):
    if rospy.has_param(f"carla_master/services/taxi/{taxi_name}/pedestrian"):
        rospy.delete_param(f"carla_master/services/taxi/{taxi_name}/pedestrian")


def select_idle_taxi_name_from_server() -> set[str]:
    from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.taxi import (
        download_registered_taxi_names,
        download_taxi_profile_from_server,
    )

    names = download_registered_taxi_names()

    if not names:
        return set()

    profiles = [download_taxi_profile_from_server(name) for name in names]

    filtered = [profile for profile in profiles if profile is not None]

    idles = [
        profile
        for profile in filtered
        if profile is not None and profile.state == VehicleState.IDLE
    ]

    if len(idles) > 0:
        return idles[0].name

    no_idle = [
        profile
        for profile in filtered
        if profile.state == VehicleState.MOVING_TO_RETURN
        or profile.state == VehicleState.ARRIVED_AT_RETURN
    ]
    sorted_no_idle = sorted(
        no_idle,
        key=lambda profile: profile.distance_to_goal,
    )

    if len(sorted_no_idle) > 0:
        return sorted_no_idle[0].name

    return set()
