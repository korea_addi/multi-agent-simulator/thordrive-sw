from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from thordrive_fms_v2.thor_carla_ros.fsm.action.vehicle import Action
    from thordrive_fms_v2.thor_carla_ros.vehicle import ThorVehicle

import rospy  # type: ignore

from thordrive_fms_v2.thor_carla_ros.fsm.action.vehicle import (
    deserialize as action_deserialize,
)
from thordrive_fms_v2.thor_carla_ros.fsm.action.vehicle import (
    serialize as action_serialize,
)
from thordrive_fms_v2.thor_carla_ros.fsm.profile.vehicle import (
    deserialize as profile_deserialize,
)
from thordrive_fms_v2.thor_carla_ros.fsm.profile.vehicle import (
    serialize as profile_serialize,
)


def register_taxi(taxi: ThorVehicle):
    name = taxi.name
    if not rospy.has_param(f"carla_master/services/taxi/{name}"):
        rospy.set_param(f"carla_master/services/taxi/{name}", dict())


def unregister_taxi(taxi: ThorVehicle):
    name = taxi.name
    if rospy.has_param(f"carla_master/services/taxi/{name}"):
        rospy.delete_param(f"carla_master/services/taxi/{name}")


def download_registered_taxi_names() -> set[str]:
    if rospy.has_param("carla_master/services/taxi"):
        return set(rospy.get_param("carla_master/services/taxi").keys())
    return set()


def upload_taxi_profile_to_server(taxi: ThorVehicle):
    name = taxi.name
    profile = taxi.get_profile()
    rospy.set_param(
        f"carla_master/services/taxi/{name}/profile", profile_serialize(profile)
    )


def download_taxi_profile_from_server(taxi_name):
    if rospy.has_param(f"carla_master/services/taxi/{taxi_name}/profile"):
        raw_data = rospy.get_param(f"carla_master/services/taxi/{taxi_name}/profile")
        return profile_deserialize(raw_data)
    return None


def upload_taxi_actions(taxi_name: str, actions: list[Action]):
    rospy.set_param(
        f"carla_master/services/taxi/{taxi_name}/actions",
        action_serialize(actions),
    )


def download_taxi_actions(taxi_name: str) -> list[Action]:
    if rospy.has_param(f"carla_master/services/taxi/{taxi_name}/actions"):
        raw_data = rospy.get_param(f"carla_master/services/taxi/{taxi_name}/actions")
        return action_deserialize(raw_data)
    return []


def clear_taxi_actions(taxi_name: str):
    if rospy.has_param(f"carla_master/services/taxi/{taxi_name}/actions"):
        rospy.delete_param(f"carla_master/services/taxi/{taxi_name}/actions")
