from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from thordrive_fms_v2.thor_carla_ros.vehicle import ThorVehicle

import rospy  # type: ignore


def register_bus(bus: ThorVehicle):
    new_name = bus.name
    rospy.set_param(f"carla_master/services/bus/{new_name}", dict())


def unregister_bus(bus: ThorVehicle):
    name = bus.name
    if rospy.has_param(f"carla_master/services/bus/{name}"):
        rospy.delete_param(f"carla_master/services/bus/{name}")


def download_registered_bus_names() -> set[str]:
    if rospy.has_param("carla_master/services/bus"):
        return set(rospy.get_param("carla_master/services/bus"))
    return set()
