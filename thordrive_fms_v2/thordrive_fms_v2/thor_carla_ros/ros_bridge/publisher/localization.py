from __future__ import annotations

import carla  # type: ignore
import rospy  # type: ignore
from custom_msgs.msg import Localization as MessageType

from thordrive_fms_v2.thor_carla_ros.ros_bridge.sim_to_ros import convert_transform


def create_message(actor: carla.Actor):
    tf = actor.get_transform()
    offset = tf.get_forward_vector()

    new_tf = carla.Transform(
        tf.location + offset,
        tf.rotation,
    )

    converted_transform = convert_transform(new_tf)
    converted_location = converted_transform.location
    converted_rotation = converted_transform.rotation

    msg = MessageType()

    msg.header.frame_id = "base"
    msg.header.stamp = rospy.Time.now()

    msg.x = converted_location.x + 500000
    msg.y = converted_location.y + 5000000
    msg.z = converted_location.z
    msg.yaw = converted_rotation.yaw
    msg.pitch = converted_rotation.pitch
    msg.roll = converted_rotation.roll

    return msg
