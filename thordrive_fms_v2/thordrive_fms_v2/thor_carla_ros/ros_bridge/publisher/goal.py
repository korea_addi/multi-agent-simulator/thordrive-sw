from __future__ import annotations

import rospy  # type: ignore
from custom_msgs.msg import Goal as MessageType


def create_message(node_id: str):
    msg = MessageType()

    msg.header.frame_id = ""
    msg.header.stamp = rospy.Time.now()

    msg.goal_type = 1
    msg.node_id = node_id

    return msg
