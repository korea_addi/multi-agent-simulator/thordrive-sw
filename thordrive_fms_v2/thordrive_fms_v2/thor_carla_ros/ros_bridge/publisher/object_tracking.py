from __future__ import annotations

from typing import List

import carla  # type: ignore
import numpy as np
import rospy  # type: ignore
from custom_msgs.msg import Edge, PerceivedObject, Wireframe
from custom_msgs.msg import PerceivedObjectList as MessageType
from geometry_msgs.msg import Point32, Pose, Twist

from thordrive_fms_v2.thor_carla_ros.math import (
    get_quaternion_from_rotation,
    get_transform_from_matrix,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.sim_to_ros import convert_transform


def create_message(ego: carla.Actor, nonego_list: List[carla.Actor]):
    msg = MessageType()

    msg.header.frame_id = "lidar"
    msg.header.stamp = rospy.Time.now()

    for nonego in nonego_list:
        preceived_object_msg = create_preceived_object_msg(ego, nonego)
        msg.objects.append(preceived_object_msg)

    return msg


def create_preceived_object_msg(ego: carla.Actor, nonego: carla.Actor):
    ego_tf = ego.get_transform()
    nonego_tf = nonego.get_transform()

    ego_inverse_matrix = ego_tf.get_inverse_matrix()
    nonego_matrix = nonego_tf.get_matrix()

    relative_nonego_matrix = np.dot(ego_inverse_matrix, nonego_matrix)
    relative_nonego_transform = get_transform_from_matrix(relative_nonego_matrix)
    converted_relative_nonego_transform = convert_transform(relative_nonego_transform)

    nonego_speed = nonego.get_velocity().length()
    converted_nonego_velocity = (
        converted_relative_nonego_transform.rotation.get_forward_vector() * nonego_speed
    )

    # Pose Msg
    pose_msg = Pose()

    relative_nonego_location = converted_relative_nonego_transform.location
    pose_msg.position.x = relative_nonego_location.x
    pose_msg.position.y = relative_nonego_location.y
    pose_msg.position.z = relative_nonego_location.z

    quat = get_quaternion_from_rotation(converted_relative_nonego_transform.rotation)
    pose_msg.orientation.x = quat[0]
    pose_msg.orientation.y = quat[1]
    pose_msg.orientation.z = quat[2]
    pose_msg.orientation.w = quat[3]

    # Twist Msg
    twist_msg = Twist()
    twist_msg.linear.x = converted_nonego_velocity.x
    twist_msg.linear.y = converted_nonego_velocity.y
    twist_msg.linear.z = converted_nonego_velocity.z
    twist_msg.angular.x = 0.0
    twist_msg.angular.y = 0.0
    twist_msg.angular.z = 0.0

    # Mesh Msg
    bounding_box = nonego.bounding_box
    bounding_box_vertices = bounding_box.get_world_vertices(
        converted_relative_nonego_transform
    )

    (
        right_back_bottom,
        right_back_top,
        left_back_bottom,
        left_back_top,
        right_front_bottom,
        right_front_top,
        left_front_bottom,
        left_front_top,
    ) = bounding_box_vertices

    center_z = (left_front_top.z + left_front_bottom.z) / 2.0

    mesh_msg = Wireframe(type="cuboid")
    mesh_msg.edges.append(Edge([0, 1]))
    mesh_msg.edges.append(Edge([1, 2]))
    mesh_msg.edges.append(Edge([2, 3]))
    mesh_msg.edges.append(Edge([3, 0]))
    mesh_msg.edges.append(Edge([4, 5]))
    mesh_msg.edges.append(Edge([5, 6]))
    mesh_msg.edges.append(Edge([6, 7]))
    mesh_msg.edges.append(Edge([7, 4]))
    mesh_msg.edges.append(Edge([0, 4]))
    mesh_msg.edges.append(Edge([1, 5]))
    mesh_msg.edges.append(Edge([2, 6]))
    mesh_msg.edges.append(Edge([3, 7]))

    mesh_msg.vertices.append(Point32(left_front_top.x, left_front_top.y, center_z))
    mesh_msg.vertices.append(Point32(left_back_top.x, left_back_top.y, center_z))
    mesh_msg.vertices.append(Point32(right_back_top.x, right_back_top.y, center_z))
    mesh_msg.vertices.append(Point32(right_front_top.x, right_front_top.y, center_z))
    mesh_msg.vertices.append(
        Point32(left_front_bottom.x, left_front_bottom.y, left_front_bottom.z)
    )
    mesh_msg.vertices.append(
        Point32(left_back_bottom.x, left_back_bottom.y, left_back_bottom.z)
    )
    mesh_msg.vertices.append(
        Point32(right_back_bottom.x, right_back_bottom.y, right_back_bottom.z)
    )
    mesh_msg.vertices.append(
        Point32(right_front_bottom.x, right_front_bottom.y, right_front_bottom.z)
    )

    classification = (
        "Car" if nonego.type_id.find("vehicle") != -1 else "MISSING"
    )  # TODO: car, truck, motorbike, bicycle

    return PerceivedObject(
        id=nonego.id,
        classification=classification,
        class_prob=1.0,
        pose=pose_msg,
        twist=twist_msg,
        mesh=mesh_msg,
    )
