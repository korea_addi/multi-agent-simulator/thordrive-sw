from __future__ import annotations

from typing import List

import carla  # type: ignore
import numpy as np
import rospy  # type: ignore
from custom_msgs.msg import TrackingObject
from custom_msgs.msg import TrackingObjectMultiArray as MessageType
from geometry_msgs.msg import Vector3

from thordrive_fms_v2.thor_carla_ros.math import (
    get_transform_from_matrix,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.sim_to_ros import (
    convert_transform,
    convert_velocity,
)


def create_message(base: carla.Actor, others: List[carla.Actor]):
    msg = MessageType()

    msg.header.frame_id = "lidar"
    msg.header.stamp = rospy.Time.now()

    ego_inverse_matrix = base.get_transform().get_inverse_matrix()

    for nonego in others:
        if base.id == nonego.id:
            continue

        sub_msg = TrackingObject()

        sub_msg.id = nonego.id
        sub_msg.label = "Car" if nonego.type_id.startswith("vehicle") else "MISSING"

        nonego_matrix = nonego.get_transform().get_matrix()
        local_nonego_matrix = np.dot(ego_inverse_matrix, nonego_matrix)
        local_nonego_transform = get_transform_from_matrix(local_nonego_matrix)
        converted_local_nonego_transform = convert_transform(local_nonego_transform)

        nonego_velocity = nonego.get_velocity()
        converted_nonego_velocity = convert_velocity(nonego_velocity)

        sub_msg.state = [
            converted_local_nonego_transform.location.x,
            converted_local_nonego_transform.location.y,
            converted_nonego_velocity.x,
            converted_nonego_velocity.y,
        ]

        nonego_bbox_extent = nonego.bounding_box.extent

        sub_msg.minmax_z = [
            converted_local_nonego_transform.location.z,
            converted_local_nonego_transform.location.z + nonego_bbox_extent.z * 2,
        ]

        converted_bounding_box_vertices = nonego.bounding_box.get_world_vertices(
            converted_local_nonego_transform
        )
        (
            right_back_bottom,
            right_back_top,
            left_back_bottom,
            left_back_top,
            right_front_bottom,
            right_front_top,
            left_front_bottom,
            left_front_top,
        ) = converted_bounding_box_vertices

        sub_msg.rect_corners.clear()
        sub_msg.rect_corners.append(Vector3(left_front_top.x, left_front_top.y, 0.0))
        sub_msg.rect_corners.append(Vector3(left_back_top.x, left_back_top.y, 0.0))
        sub_msg.rect_corners.append(Vector3(right_back_top.x, right_back_top.y, 0.0))
        sub_msg.rect_corners.append(Vector3(right_front_top.x, right_front_top.y, 0.0))

        sub_msg.corners.append(Vector3(left_front_top.x, left_front_top.y, 0.0))
        sub_msg.corners.append(Vector3(left_back_top.x, left_back_top.y, 0.0))
        sub_msg.corners.append(Vector3(right_back_top.x, right_back_top.y, 0.0))
        sub_msg.corners.append(Vector3(right_front_top.x, right_front_top.y, 0.0))

        heading = converted_local_nonego_transform.get_forward_vector()
        sub_msg.heading.x = heading.x
        sub_msg.heading.y = heading.y
        sub_msg.heading.z = heading.z

        sub_msg.dcsFeatures.valid = False

        msg.ObjectList.append(sub_msg)

    return msg
