from __future__ import annotations

import carla  # type: ignore
import rospy  # type: ignore
from nav_msgs.msg import OccupancyGrid as MessageType


def create_message(actor: carla.Actor):
    msg = MessageType()
    msg.header.frame_id = ""
    msg.header.stamp = rospy.Time.now()
    return msg
