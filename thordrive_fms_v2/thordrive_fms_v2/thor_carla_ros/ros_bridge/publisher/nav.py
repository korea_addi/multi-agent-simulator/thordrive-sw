from __future__ import annotations

import carla  # type: ignore
import numpy as np
import rospy  # type: ignore
from custom_msgs.msg import Nav as MessageType

from thordrive_fms_v2.thor_carla_ros.ros_bridge.sim_to_ros import (
    convert_acceleration,
    convert_angular_velocity,
    convert_transform,
    convert_velocity,
)


def create_message(actor: carla.Actor):
    tf = actor.get_transform()
    velocity = actor.get_velocity()
    acceleration = actor.get_acceleration()
    angular_velocity = actor.get_angular_velocity()

    converted_tf = convert_transform(tf)
    converted_velocity = convert_velocity(velocity)
    converted_acceleration = convert_acceleration(acceleration)
    converted_angular_velocity = convert_angular_velocity(angular_velocity)

    converted_loc = converted_tf.location
    converted_rot = converted_tf.rotation

    msg = MessageType()

    msg.header.frame_id = "lidar"
    msg.header.stamp = rospy.Time.now()

    msg.lat = converted_loc.y
    msg.lon = converted_loc.x
    msg.alt = converted_loc.z

    msg.roll = converted_rot.roll
    msg.pitch = converted_rot.pitch
    msg.yaw = converted_rot.yaw

    msg.v_north = converted_velocity.y
    msg.v_east = converted_velocity.x
    msg.v_down = converted_velocity.z

    msg.accel_x = converted_acceleration.x * np.cos(
        np.deg2rad(tf.rotation.yaw)
    ) - converted_acceleration.y * np.sin(np.deg2rad(tf.rotation.yaw))
    msg.accel_y = converted_acceleration.x * np.sin(
        np.deg2rad(tf.rotation.yaw)
    ) + converted_acceleration.y * np.cos(np.deg2rad(tf.rotation.yaw))
    msg.accel_z = converted_acceleration.z

    msg.roll_rate = converted_angular_velocity.x
    msg.pitch_rate = converted_angular_velocity.y
    msg.yaw_rate = converted_angular_velocity.z

    velocity_x = converted_velocity.x * np.cos(
        np.deg2rad(tf.rotation.yaw)
    ) - converted_velocity.y * np.sin(np.deg2rad(tf.rotation.yaw))
    velocity_y = converted_velocity.x * np.sin(
        np.deg2rad(tf.rotation.yaw)
    ) + converted_velocity.y * np.cos(np.deg2rad(tf.rotation.yaw))
    velocity_ = carla.Vector2D(velocity_x, velocity_y)

    msg.speed = velocity_.length()
    msg.velocity = np.sign(velocity_.x) * velocity_.length()

    control = actor.get_control()
    msg.sw_angle = control.steer * 180.0 / np.pi

    if control.gear > 0:
        msg.gear = 4
    elif control.gear < 0:
        msg.gear = 2
    else:
        msg.gear = 3

    return msg
