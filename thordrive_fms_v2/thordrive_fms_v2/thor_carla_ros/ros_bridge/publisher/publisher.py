from __future__ import annotations

from dataclasses import dataclass
from typing import TypeVar

import rospy  # type: ignore

CustomMessage = TypeVar("CustomMessage")


@dataclass
class ROSPublisher:
    topic: str
    queue_size: int
    message_type: CustomMessage
    publisher: rospy.Publisher

    def __init__(self, topic: str, queue_size: int, message_type: CustomMessage):
        self.topic = topic
        self.queue_size = queue_size
        self.message_type = message_type
        self.publisher = rospy.Publisher(
            self.topic,
            self.message_type,
            queue_size=self.queue_size,
        )

    def publish(self, message: CustomMessage):
        self.publisher.publish(message)
