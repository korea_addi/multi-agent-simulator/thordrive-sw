from __future__ import annotations

import carla  # type: ignore


def convert_location(location: carla.Location):
    return carla.Location(location.x, -location.y, location.z)


def convert_rotation(rotation: carla.Rotation):
    return carla.Rotation(-rotation.pitch, -rotation.yaw, rotation.roll)


def convert_transform(transform: carla.Transform):
    return carla.Transform(
        convert_location(transform.location), convert_rotation(transform.rotation)
    )


def convert_velocity(velocity: carla.Vector3D):
    return carla.Vector3D(velocity.x, -velocity.y, velocity.z)


def convert_acceleration(velocity: carla.Vector3D):
    return carla.Vector3D(velocity.x, -velocity.y, velocity.z)


def convert_angular_velocity(angular_velocity: carla.Vector3D):
    return carla.Vector3D(angular_velocity.x, -angular_velocity.y, -angular_velocity.z)
