from __future__ import annotations

from dataclasses import dataclass
from threading import Event

import numpy as np
import rospy  # type: ignore
from custom_msgs.msg import VehicleControl

from thordrive_fms_v2.thor_carla_ros.ros_bridge.subscriber.subscriber import (
    ROSSubscriber,
)


@dataclass
class ControlInfo:
    throttle: float = 0.0
    brake: float = 0.0
    steer: float = 0.0


class ROSControlSubscriber(ROSSubscriber):
    _output: ControlInfo

    _recipt_time: rospy.Time
    _event: Event

    def __init__(self, topic: str, queue_size: int):
        super().__init__(topic, queue_size, VehicleControl)
        self._output = VehicleControl()
        self._recipt_time = rospy.Time.now()
        self._event = Event()

    def is_timout(self, timeout):
        return (rospy.Time.now() - self._recipt_time).to_sec() > timeout

    def reset(self):
        self._output = ControlInfo()

    def get_output(self) -> ControlInfo | None:
        return self._output

    def _callback(self, msg: VehicleControl):
        self._event.set()
        self._recipt_time = msg.header.stamp
        self._output.throttle = msg.throttle
        self._output.brake = msg.brake
        self._output.steer = msg.steer / 180.0 * np.pi
