from __future__ import annotations

from typing import Any

import rospy  # type: ignore

CustomMessage = Any


class ROSSubscriber:
    _topic: str
    _queue_size: int
    _message_type: CustomMessage
    _subscriber: rospy.Subscriber

    def __init__(self, topic: str, queue_size: int, message_type: CustomMessage):
        self._topic = topic
        self._queue_size = queue_size
        self._message_type = message_type
        self._subscriber = rospy.Subscriber(
            self._topic, self._message_type, self._callback, queue_size=self._queue_size
        )

    def _callback(self, msg: CustomMessage):
        raise NotImplementedError
