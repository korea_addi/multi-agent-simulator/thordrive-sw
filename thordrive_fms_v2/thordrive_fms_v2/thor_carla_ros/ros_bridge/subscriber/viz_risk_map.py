from __future__ import annotations

from dataclasses import dataclass
from threading import Event

import numpy as np
import rospy  # type: ignore
from sensor_msgs.msg import PointCloud2

from thordrive_fms_v2.thor_carla_ros.ros_bridge.subscriber.subscriber import (
    ROSSubscriber,
)


@dataclass
class RiskInfo:
    data: np.ndarray


class ROSVizRiskMapSubscriber(ROSSubscriber):
    _output: RiskInfo

    _recipt_time: rospy.Time
    _event: Event

    def __init__(self, topic: str, queue_size: int):
        super().__init__(topic, queue_size, PointCloud2)
        self._output = PointCloud2()
        self._recipt_time = rospy.Time.now()
        self._event = Event()

    def is_timout(self, timeout):
        return (rospy.Time.now() - self._recipt_time).to_sec() > timeout

    def reset(self):
        self._output = RiskInfo()

    def get_output(self) -> RiskInfo | None:
        return self._output

    def _callback(self, msg: PointCloud2):
        self._event.set()
        self._recipt_time = msg.header.stamp
        self._output.data = msg.data
