from __future__ import annotations

from dataclasses import dataclass
from threading import Event

import rospy  # type: ignore
from custom_msgs.msg import Reference

from thordrive_fms_v2.thor_carla_ros.ros_bridge.subscriber.subscriber import (
    ROSSubscriber,
)


@dataclass
class ReferenceInfo:
    distance_to_goal: float = 0.0
    is_arrival: bool = False


class ROSReferenceSubscriber(ROSSubscriber):
    _output = ReferenceInfo()

    _recipt_time = rospy.Time()
    _event = Event()

    def __init__(self, topic: str, queue_size: int):
        super().__init__(topic, queue_size, Reference)

    def _callback(self, msg: Reference):
        self._event.set()
        self._output.distance_to_goal = msg.DIST_TO_ENDPOINT
        self._output.is_arrival = msg.arrival_signal

    def is_timout(self, timeout):
        return (rospy.Time.now() - self._recipt_time).to_sec() > timeout

    def reset(self):
        self._output = ReferenceInfo()

    def get_output(self):
        return self._output
