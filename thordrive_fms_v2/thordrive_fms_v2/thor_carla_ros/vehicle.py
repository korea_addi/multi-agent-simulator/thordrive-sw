from __future__ import annotations

from pathlib import Path
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    import carla  # type: ignore

    from thordrive_fms_v2.thor_carla_ros.fsm.action.vehicle import Action

import os
import secrets

import roslaunch  # type: ignore
import rospy  # type: ignore
from std_srvs.srv import Trigger

from thordrive_fms_v2.thor_carla_ros.actors.vehicle import CarlaVehicle
from thordrive_fms_v2.thor_carla_ros.api import CarlaAPI
from thordrive_fms_v2.thor_carla_ros.fsm.profile.vehicle import (
    Profile as VehicleProfile,
)
from thordrive_fms_v2.thor_carla_ros.fsm.state.vehicle import State as VehicleState
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.bus import (
    register_bus,
    unregister_bus,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.taxi import (
    clear_taxi_actions,
    download_taxi_actions,
    register_taxi,
    unregister_taxi,
    upload_taxi_profile_to_server,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.publisher import (
    goal,
    localization,
    nav,
    object_tracking,
    occupancy_grid,
    trajectory_prediction,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.publisher.publisher import ROSPublisher
from thordrive_fms_v2.thor_carla_ros.ros_bridge.subscriber.control import (
    ROSControlSubscriber,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.subscriber.reference import (
    ROSReferenceSubscriber,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.subscriber.viz_risk_map import (
    ROSVizRiskMapSubscriber,
)
from thordrive_fms_v2.thor_carla_ros.utils import launch_thor_modules


class ThorVehicle:
    # ROS Node Name
    _name: str

    # ROS Publishers
    _localization_pub: ROSPublisher
    _nav_pub: ROSPublisher
    _nav_xsens_pub: ROSPublisher
    _object_tracking_pub: ROSPublisher
    _trajectory_prediction_pub: ROSPublisher
    _goal_pub: ROSPublisher

    # ROS Subscribers
    _control_sub: ROSControlSubscriber
    _reference_sub: ROSReferenceSubscriber
    _viz_risk_map: ROSVizRiskMapSubscriber

    # Rate
    _rate: rospy.Rate

    # CarlaVehicle
    _carla_actor: CarlaVehicle

    # Control
    _prev_control: carla.VehicleControl

    # Thor Module
    _thor_module: roslaunch.ROSLaunch

    # State
    _state: VehicleState

    # Actions
    _actions: list[Action]

    def __init__(self, carla_actor: CarlaVehicle, name: str, hz: int):
        rospy.init_node(name)

        self._name = name
        self._rate = rospy.Rate(hz)
        self._carla_actor = carla_actor
        self._prev_control = None
        self._state = VehicleState.INIT
        self._actions = []

    def initialize(self, topic_remap: bool = False):
        remap_to = self.name if topic_remap else "/"
        self._init_publishers(remap_to)
        self._init_subscribers(remap_to)

        launch_path = str(Path(__file__).parent / "assets/thor_v2.launch")
        self._init_thor_module(launch_path, remap_to)

    def set_actions(self, actions: list[Action]):
        self._actions = actions

    def process(self):
        self._remove_risk()
        self._apply_control()
        self._run_actions()
        self._publish_all()
        self._sleep()

    def send_goal(self, node_id: str):
        msg = goal.create_message(node_id)
        self._goal_pub.publish(msg)

    @property
    def name(self) -> str:
        return self._name

    @property
    def dist_to_goal(self) -> float:
        return self._reference_sub.get_output().distance_to_goal

    @property
    def is_arrival(self) -> bool:
        return self._reference_sub.get_output().is_arrival

    def get_profile(self):
        return VehicleProfile(
            self._state,
            self.name,
            self._carla_actor.actor_id,
            self.dist_to_goal,
            self.is_arrival,
        )

    def _init_publishers(self, remap_to: str):
        self._localization_pub = ROSPublisher(
            f"{remap_to}/thor/module/localization/pose_estimator",
            1,
            localization.MessageType,
        )
        self._nav_pub = ROSPublisher(
            f"{remap_to}/thor/sensor/nav/nav_fusion", 1, nav.MessageType
        )
        self._nav_xsens_pub = ROSPublisher(
            f"{remap_to}/thor/sensor/nav/xsens/packet", 1, nav.MessageType
        )
        self._occupancy_grid_pub = ROSPublisher(
            f"{remap_to}/thor/module/occupancy2d/occupancy_grid",
            1,
            occupancy_grid.MessageType,
        )
        self._object_tracking_pub = ROSPublisher(
            f"{remap_to}/thor/module/object_tracking/output",
            1,
            object_tracking.MessageType,
        )
        self._trajectory_prediction_pub = ROSPublisher(
            f"{remap_to}/thor/module/traj_prediction/prediction_result",
            1,
            trajectory_prediction.MessageType,
        )
        self._goal_pub = ROSPublisher(
            f"{remap_to}/thor/module/globalpath_planner/goal", 1, goal.MessageType
        )

    def _init_subscribers(self, remap_to: str):
        self._control_sub = ROSControlSubscriber(
            f"{remap_to}/thor/module/low_level_controller/control_cmd", 1
        )
        self._reference_sub = ROSReferenceSubscriber(
            f"{remap_to}/thor/module/globalpath_reader/reference", 1
        )
        self._viz_risk_map = ROSVizRiskMapSubscriber(
            f"{remap_to}/thor/module/risk_assessment/viz_risk_map", 1
        )

    def _init_thor_module(self, launch_path: str, remap_to: str = ""):
        launch_args = [
            f"base:={os.environ['BASE_DIR']}",
            "area:=carla_Town03",
            "platform:=carla_mkz2017",
            f"namespace:={remap_to}",
        ]
        self._thor_module = launch_thor_modules(launch_path, launch_args)

    def _remove_risk(self):
        output = self._viz_risk_map.get_output()
        if len(output.data) > 0:
            rospy.wait_for_service("/named_risk_toggle")
            trigger = rospy.ServiceProxy("/named_risk_toggle", Trigger)
            trigger()

    def _apply_control(self):
        if self._is_arrival():
            self._carla_actor.apply_control(throttle=0.0, steer=0.0, brake=1.0)
        elif not self._control_sub.is_timout(1.0):
            control = self._control_sub.get_output()
            self._carla_actor.apply_control(
                control.throttle, control.steer, control.brake
            )
            self._prev_control = control
        elif self._prev_control:
            self._carla_actor.apply_control(
                throttle=0.0, steer=self._prev_control.steer, brake=1.0
            )

    def _is_arrival(self):
        output = self._reference_sub.get_output()
        return (
            output.distance_to_goal == 0 and output.is_arrival
        ) or output.distance_to_goal < 0.7

    def _publish_all(self):
        actor = self._carla_actor._actor
        other_actors = self._carla_actor.other_actors

        self._publish_localization(actor)
        self._publish_nav(actor)
        self._publish_nav_xsens(actor)
        self._publish_occupancy_grid(actor)
        self._publish_object_tracking(actor, other_actors)
        self._publish_trajectory_prediction(actor, other_actors)

    def _run_actions(self):
        if not self._actions:
            return

        rospy.loginfo("Running action %s", self._state.name)

        action = self._actions[0]

        if action.run(self):
            self._state = action.state
            self._actions.pop(0)

    def _publish_localization(self, actor: carla.Actor):
        msg = localization.create_message(actor)
        self._localization_pub.publish(msg)

    def _publish_nav(self, actor: carla.Actor):
        msg = nav.create_message(actor)
        self._nav_pub.publish(msg)

    def _publish_nav_xsens(self, actor: carla.Actor):
        msg = nav.create_message(actor)
        self._nav_xsens_pub.publish(msg)

    def _publish_occupancy_grid(self, actor: carla.Actor):
        msg = occupancy_grid.create_message(actor)
        self._occupancy_grid_pub.publish(msg)

    def _publish_object_tracking(self, actor: carla.Actor, others: list[carla.Actor]):
        msg = object_tracking.create_message(actor, others)
        self._object_tracking_pub.publish(msg)

    def _publish_trajectory_prediction(
        self, actor: carla.Actor, others: list[carla.Actor]
    ):
        msg = trajectory_prediction.create_message(actor, others)
        self._trajectory_prediction_pub.publish(msg)

    def _sleep(self):
        self._rate.sleep()

    def __del__(self):
        self._thor_module.stop()


class ThorBus(ThorVehicle):
    def __init__(self, name: str, spawn_tf: carla.Transform, hz: int):
        spawn_bps = CarlaAPI.blueprint_library.filter("vehicle.mitsubishi.fusorosa")
        spawn_bp = secrets.choice(spawn_bps)
        carla_actor = CarlaVehicle(spawn_bp, spawn_tf)

        super().__init__(carla_actor, f"buses_{name}", hz)
        register_bus(self)

    def __del__(self):
        unregister_bus(self)


class ThorTaxi(ThorVehicle):
    def __init__(self, name: str, spawn_tf: carla.Transform, hz: int):
        spawn_bps = CarlaAPI.blueprint_library.filter("vehicle.ford.crown")
        spawn_bp = secrets.choice(spawn_bps)
        carla_actor = CarlaVehicle(spawn_bp, spawn_tf)

        super().__init__(carla_actor, f"taxis_{name}", hz)
        register_taxi(self)

    def get_passenger_dropoff_position(
        self, offset_y: float = 10
    ) -> tuple[float, float, float]:
        right_vector = self._carla_actor._actor.get_transform().get_right_vector()
        position = self._carla_actor._actor.get_location() + right_vector * offset_y
        return position.x, position.y, position.z

    def process(self):
        super().process()
        self._upload_server()
        self._receive_actions()

    @property
    def right_vector(self):
        return self._actor.get_transform().get_right_vector()

    def _upload_server(self):
        upload_taxi_profile_to_server(self)

    def _receive_actions(self):
        actions = download_taxi_actions(self.name)
        if not actions:
            return

        self._actions = download_taxi_actions(self.name)
        clear_taxi_actions(self.name)

    def __del__(self):
        unregister_taxi(self)
