from __future__ import annotations

import json
from dataclasses import asdict, dataclass

from thordrive_fms_v2.thor_carla_ros.fsm.state.vehicle import State


def serialize(profile: Profile) -> str:
    data = asdict(profile)
    data["state"] = profile.state.value
    return json.dumps(data)


def deserialize(serialized: str) -> Profile:
    data = json.loads(serialized)
    data["state"] = State(data["state"])
    return Profile(**data)


@dataclass
class Profile:
    state: State
    name: str
    actor_id: int
    distance_to_goal: float
    is_arrival: bool
