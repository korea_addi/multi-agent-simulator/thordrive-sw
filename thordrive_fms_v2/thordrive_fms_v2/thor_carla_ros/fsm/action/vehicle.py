from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from thordrive_fms_v2.thor_carla_ros.vehicle import ThorVehicle

import json
from dataclasses import asdict, dataclass
from datetime import datetime

import numpy as np

from thordrive_fms_v2.thor_carla_ros.fsm.state.vehicle import State as VehicleState
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.master import (
    download_assigned_pedestrian_name_on_taxi_from_server,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.pedestrian import (
    is_pedestrian_onboard,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.subscriber.control import VehicleControl


def serialize(actions: list[Action]) -> str:
    datas = [asdict(action) for action in actions]
    new_datas = []
    for data in datas:
        data["state"] = data["state"].value
        new_datas.append(data)
    return json.dumps(new_datas)


def deserialize(serialized: str) -> list[Action]:
    dict_datas = json.loads(serialized)
    datas = []
    for dict_data in dict_datas:
        dict_data["state"] = VehicleState(dict_data["state"])
        datas.append(Action(**dict_data))
    return datas


@dataclass
class Action:
    state: VehicleState
    goal_node_id: str | None = None
    delay_seconds: float = 3
    start_time: datetime | None = None

    def run(self, thor_vehicle: ThorVehicle) -> bool:
        if self.state == VehicleState.IDLE:
            return True
        elif self.state == VehicleState.MOVING_TO_PICKUP:
            assert self.goal_node_id is not None

            thor_vehicle.send_goal(self.goal_node_id)

            return (
                0 < thor_vehicle.dist_to_goal < np.inf
                and thor_vehicle._control_sub.get_output() != VehicleControl()
            )
        elif self.state == VehicleState.MOVING_TO_DESTINATION:
            assert self.goal_node_id is not None

            thor_vehicle.send_goal(self.goal_node_id)

            return (
                0 < thor_vehicle.dist_to_goal < np.inf
                and thor_vehicle._control_sub.get_output() != VehicleControl()
            )
        elif self.state == VehicleState.MOVING_TO_RETURN:
            assert self.goal_node_id is not None

            thor_vehicle.send_goal(self.goal_node_id)

            return (
                0 < thor_vehicle.dist_to_goal < np.inf
                and thor_vehicle._control_sub.get_output() != VehicleControl()
            )
        elif self.state == VehicleState.ARRIVED_AT_PICKUP:
            return (
                thor_vehicle.dist_to_goal == 0 and thor_vehicle.is_arrival
            ) or thor_vehicle.dist_to_goal < 0.7
        elif self.state == VehicleState.ARRIVED_AT_DESTINATION:
            return (
                thor_vehicle.dist_to_goal == 0 and thor_vehicle.is_arrival
            ) or thor_vehicle.dist_to_goal < 0.7
        elif self.state == VehicleState.ARRIVED_AT_RETURN:
            return (
                thor_vehicle.dist_to_goal == 0 and thor_vehicle.is_arrival
            ) or thor_vehicle.dist_to_goal < 0.7
        elif self.state == VehicleState.OPEN_DOOR:
            thor_vehicle._carla_actor.open_door_front_right()
            thor_vehicle._carla_actor.open_door_back_right()
            return True
        elif self.state == VehicleState.CLOSE_DOOR:
            thor_vehicle._carla_actor.close_door_front_right()
            thor_vehicle._carla_actor.close_door_back_right()
            return True
        elif self.state == VehicleState.PICKUP_PASSENGERS:
            pedestrian_name = download_assigned_pedestrian_name_on_taxi_from_server(
                thor_vehicle._name
            )
            return is_pedestrian_onboard(pedestrian_name)
        elif self.state == VehicleState.DROP_PASSENGERS:
            pedestrian_name = download_assigned_pedestrian_name_on_taxi_from_server(
                thor_vehicle._name
            )
            return not is_pedestrian_onboard(pedestrian_name)
        elif self.state == VehicleState.DELAY:
            if self.start_time is None:
                self.start_time = datetime.now()
            return (
                datetime.now() - self.start_time
            ).total_seconds() > self.delay_seconds
        else:
            return False


def generate_bus_actions(goal_node_ids: list[str], return_node_id: str):
    jobs = [
        Action(VehicleState.MOVING_TO_PICKUP, goal_node_ids[0]),
        Action(VehicleState.ARRIVED_AT_PICKUP),
    ]

    for node_id in goal_node_ids[1:]:
        sub_jobs = [
            Action(VehicleState.OPEN_DOOR),
            Action(VehicleState.DELAY, delay_seconds=5),
            Action(VehicleState.CLOSE_DOOR),
            Action(VehicleState.DELAY, delay_seconds=5),
            Action(VehicleState.MOVING_TO_DESTINATION, node_id),
            Action(VehicleState.ARRIVED_AT_DESTINATION),
        ]
        jobs.extend(sub_jobs)

    jobs.extend(
        [
            Action(VehicleState.MOVING_TO_RETURN, return_node_id),
            Action(VehicleState.ARRIVED_AT_RETURN),
            Action(VehicleState.IDLE),
        ]
    )

    return jobs


def generate_taxi_return_actions(return_node_id: str):
    return [
        Action(VehicleState.MOVING_TO_RETURN, return_node_id),
        Action(VehicleState.ARRIVED_AT_RETURN),
        Action(VehicleState.IDLE),
    ]


def generate_taxi_actions(start_node_id: str, goal_node_id: str, return_node_id: str):
    return [
        Action(VehicleState.MOVING_TO_PICKUP, start_node_id),
        Action(VehicleState.ARRIVED_AT_PICKUP),
        Action(VehicleState.OPEN_DOOR),
        Action(VehicleState.PICKUP_PASSENGERS),
        Action(VehicleState.CLOSE_DOOR),
        Action(VehicleState.MOVING_TO_DESTINATION, goal_node_id),
        Action(VehicleState.ARRIVED_AT_DESTINATION),
        Action(VehicleState.OPEN_DOOR),
        Action(VehicleState.DROP_PASSENGERS),
        Action(VehicleState.CLOSE_DOOR),
        Action(VehicleState.MOVING_TO_RETURN, return_node_id),
        Action(VehicleState.ARRIVED_AT_RETURN),
        Action(VehicleState.IDLE),
    ]
