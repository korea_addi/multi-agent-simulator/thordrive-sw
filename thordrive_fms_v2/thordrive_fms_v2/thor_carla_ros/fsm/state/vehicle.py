from __future__ import annotations

from enum import Enum, auto


class State(Enum):
    INIT = auto()
    IDLE = auto()
    MOVING_TO_PICKUP = auto()
    MOVING_TO_DESTINATION = auto()
    MOVING_TO_RETURN = auto()
    ARRIVED_AT_PICKUP = auto()
    ARRIVED_AT_DESTINATION = auto()
    ARRIVED_AT_RETURN = auto()
    OPEN_DOOR = auto()
    CLOSE_DOOR = auto()
    PICKUP_PASSENGERS = auto()
    DROP_PASSENGERS = auto()
    DELAY = auto()
