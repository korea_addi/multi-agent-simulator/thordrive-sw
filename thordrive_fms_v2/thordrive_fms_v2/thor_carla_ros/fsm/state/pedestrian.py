from __future__ import annotations

from enum import Enum, auto


class State(Enum):
    IDLE = auto()
    WAITING = auto()
    ONBOARD = auto()
    DROPPED = auto()
    DONE = auto()
