from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from thordrive_fms_v2.thor_carla_ros.actors.pedestrian import CarlaPedestrian

import rospy  # type: ignore

from thordrive_fms_v2.thor_carla_ros.fsm.profile.pedestrian import Profile
from thordrive_fms_v2.thor_carla_ros.fsm.state.pedestrian import (
    State as PedestrianState,
)
from thordrive_fms_v2.thor_carla_ros.fsm.state.vehicle import State as VehicleState
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.master import (
    download_assigned_taxi_name_on_pedestrian_name_from_server,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.pedestrian import (
    register_pedestrian,
    set_pedestrian_offboard_to_server,
    set_pedestrian_onboard_to_server,
    unregister_pedestrian,
    upload_pedestrian_profile_to_server,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.taxi import (
    download_taxi_profile_from_server,
)


class ThorPedestrian:
    # ROS Node Name
    _name: str

    # Rate
    _rate: rospy.Rate

    # CarlaVehicle
    _carla_actor: CarlaPedestrian

    # State
    _state: PedestrianState

    # Node Ids
    _start_node_id: str
    _goal_node_id: str

    def __init__(self, carla_actor: CarlaPedestrian, name: str, hz: int):
        rospy.init_node(f"pedestrians_{name}")

        self._name = name
        self._rate = rospy.Rate(hz)
        self._carla_actor = carla_actor
        self._state = PedestrianState.IDLE

        register_pedestrian(self)

    def set_start_node_id(self, start_node_id: str):
        self._start_node_id = start_node_id

    def set_goal_node_id(self, goal_node_id: str):
        self._goal_node_id = goal_node_id

    def try_onboard(self) -> bool:
        if self.ready_to_onboard():
            self._state = PedestrianState.ONBOARD
            upload_pedestrian_profile_to_server(self)
            set_pedestrian_onboard_to_server(self)
            return True
        return False

    def ready_to_onboard(self) -> bool:
        taxi_name = self.get_assigned_taxi_name()
        if taxi_name is None:
            return False

        taxi_profile = download_taxi_profile_from_server(taxi_name)
        return taxi_profile.state == VehicleState.OPEN_DOOR

    def try_offboard(self) -> bool:
        if self.ready_to_onboard():
            self._state = PedestrianState.DROPPED
            upload_pedestrian_profile_to_server(self)
            set_pedestrian_offboard_to_server(self)
            return True
        return False

    def ready_to_offboard(self) -> bool:
        taxi_name = self.get_assigned_taxi_name()
        if taxi_name is None:
            return False

        taxi_profile = download_taxi_profile_from_server(taxi_name)
        return taxi_profile.state == VehicleState.OPEN_DOOR

    def process(self):
        self._upload_server()
        self._sleep()

    def get_state(self):
        return self._state

    def get_profile(self):
        return Profile(
            self._state,
            self.name,
            self._start_node_id,
            self._goal_node_id,
            self.get_assigned_taxi_name(),
        )

    def get_assigned_taxi_name(self):
        return download_assigned_taxi_name_on_pedestrian_name_from_server(self._name)

    def hide(self):
        self._carla_actor.hide()

    def show(self, x: float, y: float, z: float):
        self._carla_actor.show(x, y, z)

    @property
    def name(self) -> str:
        return self._name

    def _upload_server(self):
        upload_pedestrian_profile_to_server(self)

    def _sleep(self):
        self._rate.sleep()

    def __del__(self):
        unregister_pedestrian(self)
