from __future__ import annotations

import names
import roslaunch  # type: ignore


def generate_random_name() -> str:
    return names.get_first_name()


def launch_thor_modules(path: str, args: list[str], debug=False):
    launch_files = [(path, args)]

    # starting a launch file
    uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    roslaunch.configure_logging(uuid)
    launch = roslaunch.scriptapi.ROSLaunch()
    launch.parent = roslaunch.parent.ROSLaunchParent(
        uuid, launch_files, force_screen=debug, force_log=True
    )

    launch.start()

    return launch
