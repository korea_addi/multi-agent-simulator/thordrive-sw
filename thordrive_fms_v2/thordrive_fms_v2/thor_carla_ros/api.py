from __future__ import annotations

import secrets
import socket
from dataclasses import dataclass

import carla  # type: ignore
import numpy as np

from thordrive_fms_v2.thor_carla_ros.math import get_transform_from_matrix


@dataclass
class VehicleBlueprintProfile:
    id: str
    blueprint: carla.ActorBlueprint
    bounding_box: carla.BoundingBox


class CarlaAPI:
    client: carla.Client
    world: carla.World
    map: carla.Map
    blueprint_library: carla.BlueprintLibrary
    traffic_manager: carla.TrafficManager
    debug_helper: carla.DebugHelper

    _blueprint_profiles: list[VehicleBlueprintProfile]

    @staticmethod
    def connect(ip: str, port: int, map_name: str | None = None):
        CarlaAPI.client = carla.Client(ip, port)

        if map_name is None:
            CarlaAPI.world = CarlaAPI.client.get_world()
        else:
            CarlaAPI.world = CarlaAPI.client.load_world(map_name)

        CarlaAPI.map = CarlaAPI.world.get_map()
        CarlaAPI.blueprint_library = CarlaAPI.world.get_blueprint_library()
        CarlaAPI.traffic_manager = CarlaAPI.client.get_trafficmanager(
            CarlaAPI._get_free_port()
        )
        CarlaAPI.debug_helper = CarlaAPI.world.debug
        CarlaAPI.world.set_weather(carla.WeatherParameters.ClearNoon)

        CarlaAPI._init_vehicle_blueprint_profile()

    @staticmethod
    def set_sync(dt: float = 0.05):
        CarlaAPI.traffic_manager.set_synchronous_mode(True)
        settings = CarlaAPI.world.get_settings()
        settings.synchronous_mode = True
        settings.fixed_delta_seconds = dt
        CarlaAPI.world.apply_settings(settings)

    @staticmethod
    def set_async():
        CarlaAPI.traffic_manager.set_synchronous_mode(False)
        settings = CarlaAPI.world.get_settings()
        settings.synchronous_mode = False
        settings.fixed_delta_seconds = 0.0
        CarlaAPI.world.apply_settings(settings)

    @staticmethod
    def get_actor(id: str):
        return CarlaAPI.world.get_actor(id)

    @staticmethod
    def get_vehicle_spawn_points():
        return CarlaAPI.map.get_spawn_points()

    @staticmethod
    def get_vehicle_spawn_point(location: carla.Location):
        tf = CarlaAPI.map.get_waypoint(
            location, lane_type=carla.LaneType.Driving
        ).transform
        tf.location.z += 2.0
        return tf

    @staticmethod
    def get_pedestrian_spawn_point(location: carla.Location):
        tf = CarlaAPI.map.get_waypoint(
            location,
            lane_type=carla.LaneType.Sidewalk,
        ).transform
        tf.location.z += 2.0
        return tf

    @staticmethod
    def get_waypoint(location: carla.Location):
        waypoint = CarlaAPI.map.get_waypoint(location)
        if waypoint:
            return waypoint
        raise RuntimeError()

    @staticmethod
    def get_spectator():
        return CarlaAPI.world.get_spectator()

    @staticmethod
    def set_spectator(actor: carla.Actor):
        tf = actor.get_transform()
        loc = tf.location
        rot = tf.rotation
        bbox = actor.bounding_box

        global_location = loc + bbox.location
        global_rotation = carla.Rotation(yaw=rot.yaw + bbox.rotation.yaw)
        global_transform = carla.Transform(global_location, global_rotation)

        relative_location = carla.Location(-8 * bbox.extent.x, 0.0, 7 * bbox.extent.z)
        relative_rotation = carla.Rotation(pitch=15.0)
        relative_transform = carla.Transform(relative_location, relative_rotation)

        spectator_matrix = np.dot(
            global_transform.get_matrix(),
            relative_transform.get_matrix(),
        )
        spectator_transform = get_transform_from_matrix(spectator_matrix)

        CarlaAPI.get_spectator().set_transform(spectator_transform)

    @staticmethod
    def tick(count: int = 10):
        CarlaAPI.world.tick(count)

    @staticmethod
    def find_blueprint(bp_id: str):
        return CarlaAPI.world.get_blueprint_library().find(bp_id)

    @staticmethod
    def filter_blueprint(bp_wildcard_pattern: str):
        return CarlaAPI.world.get_blueprint_library().filter(bp_wildcard_pattern)

    @staticmethod
    def get_traffic_manager_port():
        return CarlaAPI.traffic_manager.get_port()

    @staticmethod
    def get_vehicle_blueprint_profiles(
        blueprint_id: str | None = None,
        excluded_bp_ids: tuple[str] | None = None,
        role_name: str | None = None,
    ):
        if blueprint_id is not None:
            return list(
                filter(lambda p: p.id == blueprint_id, CarlaAPI._blueprint_profiles)
            )

        candidates = CarlaAPI._blueprint_profiles
        if excluded_bp_ids is not None:
            candidates = list(filter(lambda p: p.id not in excluded_bp_ids, candidates))

        selected = secrets.choice(candidates)

        if role_name is not None:
            bp = CarlaAPI.blueprint_library.find(selected.id)
            bp.set_attribute("role_name", role_name)
            return [VehicleBlueprintProfile(selected.id, bp, selected.bounding_box)]

        return [selected]

    @staticmethod
    def spawn_actor(
        transform: carla.Transform,
        bp: carla.ActorBlueprint = None,
        bp_id: str | None = None,
        bp_wildcard_pattern: str | None = None,
        attach_to=None,
    ):
        if bp:
            return CarlaAPI.world.spawn_actor(bp, transform, attach_to=attach_to)
        elif bp_id:
            bp = CarlaAPI.find_blueprint(bp_id)
            return CarlaAPI.world.spawn_actor(bp, transform, attach_to=attach_to)
        elif bp_wildcard_pattern:
            bps = CarlaAPI.filter_blueprint(bp_wildcard_pattern)
            bp = np.random.choice(bps)
            return CarlaAPI.world.spawn_actor(bp, transform, attach_to=attach_to)
        raise RuntimeError

    @staticmethod
    def try_spawn_actor(
        transform: carla.Transform,
        bp: carla.ActorBlueprint = None,
        bp_id: str | None = None,
        bp_wildcard_pattern: str | None = None,
        attach_to=None,
    ):
        if bp:
            return CarlaAPI.world.try_spawn_actor(bp, transform, attach_to=attach_to)
        elif bp_id:
            bp = CarlaAPI.find_blueprint(bp_id)
            return CarlaAPI.world.try_spawn_actor(bp, transform, attach_to=attach_to)
        elif bp_wildcard_pattern:
            bps = CarlaAPI.filter_blueprint(bp_wildcard_pattern)
            bp = np.random.choice(bps)
            return CarlaAPI.world.try_spawn_actor(bp, transform, attach_to=attach_to)
        raise RuntimeError

    @staticmethod
    def destroy(actor: tuple[carla.Actor, list[carla.Actor], None]):
        if actor is None:
            return
        elif isinstance(actor, list):
            destroy_actors = [carla.command.DestroyActor(a) for a in actor]
            CarlaAPI.client.apply_batch(destroy_actors)
        else:
            actor.destroy()

    def draw_box(actor: carla.Actor, thickness=0.1, color=(255, 0, 0), life_time=-1.0):
        bounding_box = actor.bounding_box
        transform = actor.get_transform()

        bounding_box.location = transform.location + carla.Location(
            z=bounding_box.extent.z
        )

        CarlaAPI.debug_helper.draw_box(
            bounding_box,
            transform.rotation,
            thickness,
            carla.Color(*color),
            life_time,
        )

    @staticmethod
    def _init_vehicle_blueprint_profile():
        CarlaAPI._blueprint_profiles = []

        half_map_width = 1000
        map_height = 10000

        all_bps = []
        all_actors = []
        all_bounding_boxes = []

        for bp in CarlaAPI.blueprint_library.filter("vehicle.*"):
            if bp.get_attribute("number_of_wheels").as_int() == 4:
                all_bps.append(bp)

        xs = np.linspace(-half_map_width, half_map_width, len(all_bps))
        y = 0
        z = map_height

        for x, bp in zip(xs, all_bps):
            transform = carla.Transform(carla.Location(x, y, z))
            actor = CarlaAPI.world.try_spawn_actor(bp, transform)
            all_actors.append(actor)

        CarlaAPI.world.tick()

        for actor in all_actors:
            bounding_box = actor.bounding_box
            all_bounding_boxes.append(bounding_box)

        CarlaAPI.destroy(all_actors)

        for bp, bounding_box in zip(all_bps, all_bounding_boxes):
            CarlaAPI._blueprint_profiles.append(
                VehicleBlueprintProfile(bp.id, bp, bounding_box)
            )

    @staticmethod
    def _get_free_port():
        with socket.socket() as s:
            s.bind(("", 0))
            _, port = s.getsockname()
            return port
