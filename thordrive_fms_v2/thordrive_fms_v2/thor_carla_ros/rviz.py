from __future__ import annotations

import copy
import os
import shlex
import subprocess
from typing import Any

import yaml

__process__: subprocess.Popen | None = None


def terminate():
    global __process__

    if __process__ is not None:
        __process__.terminate()
        __process__.wait()
        __process__ = None


def launch(client_ids: set[str]):
    global __process__

    terminate()

    new_config_path = os.path.join(os.path.dirname(__file__), "assets/.rviz")

    with open(new_config_path, "w+") as f:
        config_data = get_rviz_config_data(client_ids)
        yaml.dump(config_data, f)

    command = f"rviz -d {new_config_path}"
    command_list = shlex.split(command)

    __process__ = subprocess.Popen(
        command_list,
        stderr=subprocess.PIPE,
    )


def get_rviz_config_data(client_ids: set[str]):
    template_path = os.path.join(os.path.dirname(__file__), "assets/template.rviz")

    with open(template_path) as f:
        template_config = yaml.safe_load(f)

    new_config = copy.deepcopy(template_config)

    template_rviz_group = extract_rviz_group(template_config)

    clear_rviz_group(new_config)

    for client_id in sorted(list(client_ids)):
        new_rviz_group = copy.deepcopy(template_rviz_group)
        update_displays(new_rviz_group, client_id)
        new_config["Visualization Manager"]["Displays"].append(new_rviz_group)

    return new_config


def extrace_rviz_displays(config: Any) -> Any:
    return config["Visualization Manager"]["Displays"]


def extract_rviz_group(config: Any) -> list:
    for a in extrace_rviz_displays(config):
        if a["Class"] == "rviz/Group":
            return a
    raise ValueError("No display template found")


def clear_rviz_group(config: Any):
    idx = []

    for i, display in enumerate(config["Visualization Manager"]["Displays"]):
        if display["Class"] == "rviz/Group":
            idx.append(i)

    for i in reversed(idx):
        config["Visualization Manager"]["Displays"].pop(i)


def add_new_group(name, display_group):
    return {
        "Class": "rviz/Group",
        "Displays": display_group,
        "Enabled": False,
    }


def update_displays(displays, client_id: str):
    displays["Name"] = client_id

    for display in displays["Displays"]:
        if "Marker Topic" in display:
            old_topic: str = display["Marker Topic"]
            new_topic = f"/{client_id}/{old_topic[old_topic.find('thor') :]}"
            display["Marker Topic"] = new_topic
        if "Topic" in display:
            old_topic = display["Topic"]
            new_topic = f"/{client_id}/{old_topic[old_topic.find('thor') :]}"
            display["Topic"] = new_topic
