from __future__ import annotations

import rospy  # type: ignore

from thordrive_fms_v2.thor_carla_ros.server import ROSServer


def main():
    return_node_id = "1189012300"

    ros_server = ROSServer("carla_server", hz=33)
    ros_server.set_environment()
    ros_server.set_taxi_return_node_id(return_node_id)

    rospy.loginfo("Initializing server")

    while not rospy.is_shutdown():
        ros_server.process()
        ros_server.tick()
        ros_server.sleep()
