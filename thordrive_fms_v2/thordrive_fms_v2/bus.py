from __future__ import annotations

import carla  # type: ignore
import rospy  # type: ignore

from thordrive_fms_v2.thor_carla_ros.api import CarlaAPI
from thordrive_fms_v2.thor_carla_ros.fsm.action.vehicle import generate_bus_actions
from thordrive_fms_v2.thor_carla_ros.render import PyGameRender
from thordrive_fms_v2.thor_carla_ros.vehicle import ThorBus


def main():
    name = "T8438"
    location = carla.Location(81, -110)

    station_ids = [
        "1189020300",
        "1189018000",
        "1189001600",
        "1189005500",
        "1189010900",
        "1189013800",
        "1189016400",
    ] * 10
    return_id = "1189016900"

    CarlaAPI.connect(ip="localhost", port=2000)
    spawn_tf = CarlaAPI.get_vehicle_spawn_point(location)

    thor_client = ThorBus(name, spawn_tf, hz=10)
    renderer = PyGameRender(thor_client._carla_actor, name)

    actions = generate_bus_actions(station_ids, return_id)
    thor_client.set_actions(actions)

    thor_client.initialize(topic_remap=True)

    while not rospy.is_shutdown():
        renderer.render()
        thor_client.process()


if __name__ == "__main__":
    main()
