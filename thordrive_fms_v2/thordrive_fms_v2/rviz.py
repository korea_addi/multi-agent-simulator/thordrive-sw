from __future__ import annotations

import time

import rospy  # type: ignore

from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.bus import (
    download_registered_bus_names,
)
from thordrive_fms_v2.thor_carla_ros.ros_bridge.mediator.taxi import (
    download_registered_taxi_names,
)
from thordrive_fms_v2.thor_carla_ros.rviz import launch as rviz_launch
from thordrive_fms_v2.thor_carla_ros.rviz import terminate as rviz_terminate


def main():
    rospy.init_node("carla_rviz")

    cached_names = set()

    while not rospy.is_shutdown():
        bus_names = download_registered_bus_names()
        taxi_names = download_registered_taxi_names()

        all_names = bus_names | taxi_names

        if cached_names != all_names:
            rospy.loginfo("Client data changed...")
            rospy.loginfo("Relaunching RViz...")
            cached_names = all_names
            rviz_launch(all_names)
        elif not cached_names:
            rospy.loginfo("No clients found...")
            rospy.loginfo("Waiting for clients...")
            rospy.loginfo("Terminating RViz...")
            rviz_terminate()

        time.sleep(1)


if __name__ == "__main__":
    main()
