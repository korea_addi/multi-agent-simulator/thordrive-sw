import sys
from pathlib import Path

current_dir = Path(__file__).parent
dists_dir = current_dir / ".." / "dists"
carla_egg_path = dists_dir / "carla-0.9.14-py3.7-linux-x86_64.egg"

sys.path.append(str(dists_dir))
sys.path.append(str(carla_egg_path))
