import curses

import carla  # type: ignore
import rospy  # type: ignore

from thordrive_fms_v2.thor_carla_ros.actors.pedestrian import CarlaPedestrian
from thordrive_fms_v2.thor_carla_ros.api import CarlaAPI
from thordrive_fms_v2.thor_carla_ros.pedestrian import ThorPedestrian
from thordrive_fms_v2.thor_carla_ros.render import PyGameRender
from thordrive_fms_v2.thor_carla_ros.utils import generate_random_name


def main():
    name = generate_random_name()
    start_node_location = carla.Location(64, 9)
    start_node_id = "1189003200"
    goal_node_id = "1189010300"

    CarlaAPI.connect(ip="localhost", port=2000)

    spawn_tf = CarlaAPI.get_pedestrian_spawn_point(start_node_location)
    spawn_tf.location.y = 11
    spawn_tf.rotation.yaw -= 90

    carla_client = CarlaPedestrian(spawn_tf)
    thor_client = ThorPedestrian(carla_client, name, hz=10)
    renderer = PyGameRender(thor_client._carla_actor, name)

    thor_client.set_start_node_id(start_node_id)
    thor_client.set_goal_node_id(goal_node_id)

    def keyboard_func(stdscr: curses.window):
        nonlocal thor_client
        nonlocal renderer

        is_onboard = False
        is_offboard = False

        curses.cbreak()
        stdscr.keypad(True)
        stdscr.nodelay(True)

        stdscr.addstr("Let's try boarding by pressing the space key. \n")
        stdscr.refresh()

        while not rospy.is_shutdown():
            renderer.render()
            thor_client.process()

            key = stdscr.getch()

            if key == 32:
                if is_onboard is False:
                    stdscr.addstr("Attempt to board. \n")
                    if thor_client.try_onboard():
                        stdscr.addstr("Boarded. \n")
                    else:
                        stdscr.addstr("Boarding failed. \n")
                elif is_offboard is False:
                    stdscr.addstr("Try to get off. \n")
                    if thor_client.try_offboard():
                        stdscr.addstr("Got off. \n")
                        break
                    else:
                        stdscr.addstr("Get off failed. \n")

            stdscr.refresh()

    curses.wrapper(keyboard_func)
