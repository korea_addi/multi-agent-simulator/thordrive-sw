# KETI-sim FMS Overview

This repository provides a Fleet Management System (FMS) designed to manage and coordinate multiple vehicles. It includes two distinct versions of the system, each tailored for different features and environments.


## Versions

### 1. thordrive_fms_v1

This is the first version of the Fleet Management System, providing fundamental capabilities for managing vehicle fleets. It is designed for general-purpose fleet management without specific integration with simulation tools like CARLA.


#### Key Features:

- Basic fleet management system for multiple vehicles.
- Supports multiple vehicles.
- Tested for use on Ubuntu 16.04, 18.04, and 20.04.


#### Setup and Usage:

To get started with this version, navigate to the thordrive_fms_v1 folder and refer to its [README](./thordrive_fms_v1/README.md) for detailed instructions on installation and usage:

```
cd thordrive_fms_v1
```


### 2. thordrive_fms_v2

This is the enhanced version of the Fleet Management System, integrating with the CARLA Simulator for realistic vehicle behavior and supporting advanced scenarios like taxi and bus fleet operations.


#### Key Features:

- Integration with the CARLA Simulator (version 0.9.14).
- Support for advanced scenarios such as taxi and bus fleet operations.
- Designed for use on Ubuntu 20.04 with Python 3.8+.


#### Setup and Usage:

To get started with this version, navigate to the thordrive_fms_v2 folder and refer to its [README](./thordrive_fms_v2/README.md) for detailed instructions on installation and usage:

```
cd thordrive_fms_v2
```


### Folder Structure

The repository is organized as follows:

```
.
├── README.md               # Overview of both versions
├── thordrive_fms_v1        # Version 1 of the FMS
│   ├── README.md               # Instructions for version 1
│   ├── pyproject.toml          # Poetry configuration
│   └── ...                     # Source code and modules
└── thordrive_fms_v2        # Version 2 of the FMS         
    ├── README.md               # Instructions for version 2     
    ├── pyproject.toml          # Poetry configuration         
    └── ...                     # Source code and modules
```


### Notes

- Ensure you meet the system and dependency requirements specified in each version’s README file.
- The two versions are independent and serve different purposes. Choose the version that fits your needs based on whether you require basic fleet management or advanced simulation capabilities with CARLA.
